import json

from celery.signals import task_postrun, task_prerun

from shipka.persistance import db
from shipka.persistance.models.tasks import Task
from shipka.workers import worker, task_login_user


@task_prerun.connect
def before_task(task_id=None, task=None, *args, **kwargs):
    task.flask_app.app_context().push()
    task.flask_app.test_request_context().push()
    task_login_user(kwargs.get('kwargs', {}).get('logged_user_id'))


@task_postrun.connect
def after_task(task_id=None, task=None, retval=None, state=None, *args, **kwargs):
    task_instance = Task.get_one_by(uuid=task_id)

    task_instance.update(**dict(
        status=state,
        result=json.dumps(retval)
    ))
    db.push()


@worker.task
def sum_args(x, y):
    return str(x + y)
