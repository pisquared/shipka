import sys

from celery import Celery


def task_login_user(user_id):
    from flask_security import current_user, login_user
    from shipka.persistance.models.auth import User
    user = User.get_one_by(id=int(user_id))
    login_user(user)
    print(current_user.id)


def make_celery():
    from shipka.webapp import configure, init_socketio
    from shipka.webapp.auth import init_security
    from shipka.persistance import init_db, init_search
    from flask import Flask, current_app

    if current_app:
        app = current_app
    else:
        app = Flask(__name__)
        configure(app)
        init_db(app)
        init_search(app)
        init_security(app)
        init_socketio(app)

    _celery = Celery(app.import_name,
                     broker=app.config['CELERY_BROKER_URL'])
    _celery.conf.result_backend = app.config['CELERY_RESULT_BACKEND']
    del app.config['CELERY_RESULT_BACKEND']
    _celery.conf.update(app.config)
    TaskBase = _celery.Task

    class ContextTask(TaskBase):
        abstract = True
        flask_app = app

        def __call__(self, *args, **kwargs):
            # with app.app_context():
            #     with app.test_request_context():
            return TaskBase.__call__(self, *args, **kwargs)

    _celery.Task = ContextTask

    return _celery, app


worker, app = make_celery()

# register tasks
if 'shipka.workers.tasks' not in sys.modules.keys():
    from shipka.workers import tasks

if 'workers.tasks' not in sys.modules.keys():
    with app.app_context():
        # noinspection PyUnresolvedReferences
        from workers import tasks
        tasks.init_once()
