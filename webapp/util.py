import json
import re

import datetime
import pytz as pytz
from dateutil import tz
from flask import url_for, render_template, render_template_string
from dateutil.parser import parse as dateparse

from markupsafe import escape, Markup


def safe_list(_list):
    class SafeList(list):
        def get(self, index):
            try:
                return self.__getitem__(index)
            except IndexError:
                return None

    return SafeList(_list)


def camel_case_to_snake_case(name):
    """
    Convertes a CamelCase name to snake_case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def get_now_utc(tzspecific=True):
    """
    Helper function to avoid all confusion about datetime.utcnow
    :param tzspecific: should we return Timezone specific datetime (usually yes)
    :return:
    """
    if tzspecific:
        return datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    return datetime.datetime.utcnow()


def get_user_tz(user):
    return tz.tzoffset(None, datetime.timedelta(seconds=user.tz_offset_seconds))


def get_now_user(user):
    """
    Gets the user specific time based on his timezone
    """
    tzinfo = get_user_tz(user)
    return datetime.datetime.utcnow().replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def format_seconds_to_human(seconds):
    if not seconds:
        return "0 minutes"
    hours = seconds / 3600.0
    minutes = seconds / 60
    minutes_repr = '%d %s' % (minutes, "minute" if minutes == 1.0 else "minutes")
    if hours >= 1.0:
        hours_repr = '%d %s' % (int(hours), "hour" if int(hours) == 1 else "hours")
        minutes = (seconds - int(hours) * 3600) / 60.0
        minutes_repr = '%d %s' % (minutes, "minute" if minutes == 1.0 else "minutes")
        if minutes > 0.0:
            return '%s and %s' % (hours_repr, minutes_repr)
        return '%s' % hours_repr
    return minutes_repr


def parse_dt_strategy(value, parser_f):
    from shipka.persistance.models.auth import get_user
    dt = None
    if value:
        try:
            user = get_user()
            dt = parser_f(value).replace(tzinfo=get_user_tz(user))
        except (TypeError, ValueError):
            dt = None
    return dt


def parse_dt(value):
    return parse_dt_strategy(value, dateparse)


def serialize_request_form(allowed_keys, payload):
    rv = {}
    for k, v in payload.iteritems():
        is_bool = k.endswith('_bool')
        if is_bool:
            k = k.split('_bool')[0]
            is_bool = True
        if k not in ['csrf_token', ] and k in allowed_keys:
            if type(v) is str and v.isdigit():
                v = int(v)
            if k.endswith('_ts'):
                v = parse_dt(v)
            if is_bool:
                v = bool(v)
            rv[k] = v
    return rv


_paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')


def url_for_view(url_name, **kwargs):
    return url_for('get_user_view', url_name=url_name, **kwargs)


def render_element(**ctx):
    model = ctx.pop('model')
    instance = ctx.pop('instance')
    field = ctx.pop('field')
    ctx['wrapping_element'] = ctx.get("wrapping_element", "div")
    ctx['read_element'] = "span"
    ctx['submit_content'] = "Submit"
    ctx['cancel_content'] = "<i class=\"fa fa-times\"></i>"
    ctx['form_classes'] = 'form-field-update'
    read_value = getattr(instance, field)
    input_elements = [render_template("macros/input_element.html",
                                      input_type="text",
                                      input_name=field,
                                      input_value=read_value,
                                      kwargs=ctx)]
    return render_template("macros/model_form.html",
                           model=model, instance=instance, field=field,
                           read_value=read_value, input_elements=input_elements,
                           kwargs=ctx)


def render_form(**ctx):
    model = ctx.pop('model')
    fields = ctx.pop('fields')
    ctx['wrapping_element'] = ctx.get("wrapping_element", "div")
    ctx['read_element'] = "span"
    ctx['submit_content'] = "Submit"
    ctx['cancel_content'] = "<i class=\"fa fa-times\"></i>"
    input_elements = []

    for field in fields:
        input_elements.append(render_template("macros/input_element.html",
                                              input_type="text",
                                              input_name=field,
                                              input_value='',
                                              kwargs=ctx))
    return render_template("macros/model_form.html",
                           model=model, field=field,
                           read_value='',
                           input_elements=input_elements,
                           kwargs=ctx)


def render_notification(notification):
    return render_template_string(notification.message_template,
                                  **json.loads(notification.message_context))


# TODO - find a library to do that
LANG_PROPS = {
    "en": {"flag": "us", "name": "English", "local_name": "English"},
    "bg": {"flag": "bg", "name": "Bulgarian", "local_name": "Български"},
    "de": {"flag": "de", "name": "German", "local_name": "Deutch"},
}


def create_jinja_helpers(app):
    @app.template_filter('seconds_to_human')
    def seconds_to_human(seconds):
        return format_seconds_to_human(seconds)

    @app.template_filter('float_to_tz')
    def float_to_tz(hours):
        int_hours = int(hours)
        minutes = int((hours - int_hours) * 60)
        return "{}{:02d}:{:02d}".format('+' if int_hours >= 0 else '-', int_hours, minutes)

    @app.template_filter('format_dt')
    def format_datetime(dt, formatting="%A, %d %b %Y"):
        """
        Formats the datetime string provided in value into whatever format you want that is supported by python strftime
        http://strftime.org/
        :param formatting The specific format of the datetime
        :param dt a datetime object
        :return:
        """
        return dt.strftime(formatting)

    @app.template_filter('nl2br')
    def nl2br(text):
        text = escape(text)
        result = u'<br>'.join(u'%s' % p.replace('\n', '<br>\n') for p in _paragraph_re.split(text))
        return Markup(result)

    @app.template_filter('hl_search')
    def highlight(text, query, n_hl=3, all_if_no_match=False):
        snippets = []
        for word in query.split():
            match_positions = [m.start() for m in re.finditer(word, text, flags=re.IGNORECASE)]
            for mpos in match_positions:
                match_before = ' '.join(text[:mpos].split()[-3:])
                match = text[mpos:mpos + len(word)]
                match_after = ' '.join(text[mpos:].split()[1:4])
                snippet = "%s <b>%s</b> %s" % (escape(match_before), escape(match),
                                               escape(match_after))
                snippets.append(snippet)
        if not snippets and all_if_no_match:
            return text
        return '...'.join(snippets[:n_hl])

    @app.template_filter('lang_props')
    def lang_props(lang, prop):
        return LANG_PROPS.get(lang, {}).get(prop)

    app.jinja_env.globals.update(url_for_view=url_for_view)
    app.jinja_env.globals.update(render_element=render_element)
    app.jinja_env.globals.update(render_form=render_form)
    app.jinja_env.globals.update(render_notification=render_notification)


def granulate_time(seconds):
    intervals = (
        ('days', 86400),  # 60 * 60 * 24
        ('hours', 3600),  # 60 * 60
        ('minutes', 60),
        ('seconds', 1),
    )
    rv = {}
    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
        rv[name] = value
    return rv


def human_time(granular_time):
    rv = "{:02d}:{:02d}".format(granular_time["minutes"], granular_time["seconds"])
    if granular_time["hours"]:
        rv = "{}:{}".format(granular_time["hours"], rv)
    if granular_time["days"]:
        rv = "{}d{}".format(granular_time["days"], rv)
    return rv


def transform_time(value):
    total_s = int(value)
    granular = granulate_time(total_s)
    human = human_time(granular)
    return {
        "total_seconds": total_s,
        "granular": granular,
        "human": human,
    }


def transform_time_interval(value):
    if value and "/" in value:
        start_s, end_s = value.split("/")
        start, end = transform_time(start_s), transform_time(end_s)
        return {
            "start": start,
            "end": end,
        }
    return {"value": value}
