from flask import request, render_template
from flask_login import current_user
from flask_socketio import join_room, emit

from shipka.persistance.database import db
from shipka.persistance.models.models import ModelDefinition
from shipka.webapp import socketio
from shipka.webapp.util import get_now_user

from webapp.bp_client import bp_client


@bp_client.route('/websockets')
def websockets():
    return render_template("websockets.html")


@socketio.on('SYN')
def socket_connect(json):
    payload = {'request_sid': request.sid, }
    if current_user.is_authenticated:
        tz_offset_minutes = json.get('tz_offset_minutes')
        tz_offset_seconds = -(tz_offset_minutes * 60)
        if current_user.tz_offset_seconds != tz_offset_seconds:
            current_user.tz_offset_seconds = tz_offset_seconds
            db.add(current_user)
            db.push()
        model_definitions = ModelDefinition.get_all()
        models = []
        for md in model_definitions:
            models.append({
                'model_name': md.name,
                'latest_created': md.latest_created,
                'latest_updated': md.latest_updated,
                'latest_deleted': md.latest_deleted,
            })
        join_room(current_user.id)
        payload.update({'user': {'id': current_user.id, 'tz_offset_seconds': current_user.tz_offset_seconds},
                        'datetime': str(get_now_user(current_user)),
                        'models': models
                        })
    print('< SYN: ' + str(json))
    print('> ACK: ' + str(payload))
    emit('ACK', payload)
