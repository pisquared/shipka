'use strict';

let cacheVersion = 1;
let currentCache = {
  offline: 'offline-cache' + cacheVersion
};

let urlsToCache = [
  '/',
  '/static/dev/css/vendor/bootstrap-duration-picker.css',
  '/static/dev/css/vendor/bootstrap-datetime-picker.css',
  '/static/dev/css/vendor/bootstrap-select.css',
  '/static/dev/css/vendor/bootstrap-table.css',
  '/static/dev/css/vendor/fontawesome.css',
  '/static/dev/css/vendor/bootstrap.css',
  '/static/dev/js/vendor/jquery.js',
  '/static/dev/js/vendor/underscore.js',
  '/static/dev/js/vendor/backbone.js',
  '/static/dev/js/vendor/backbone.localStorage.js',
  '/static/dev/js/vendor/moment.js',
  '/static/dev/js/vendor/popper.js',
  '/static/dev/js/vendor/bootstrap.js',
  '/static/dev/js/vendor/bootstrap-duration-picker.js',
  '/static/dev/js/vendor/bootstrap-datetime-picker.js',
  '/static/dev/js/vendor/bootstrap-select.js',
  '/static/dev/js/vendor/bootstrap-table.js',
  '/static/dev/js/vendor/socket.io.js',
  '/static/dev/js/vendor/vue.js',
  '/static/dev/js/vendor/emojione.js',
  '/static/dev/js/vendor/dragula.js',
];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(currentCache.offline).then(function (cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('fetch', (event) => {
  event.respondWith(async function() {
    try {
      return await fetch(event.request);
    } catch (err) {
      return caches.match(event.request);
    }
  }());
});