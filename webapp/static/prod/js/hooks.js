$('.form-field-update').hide();
$('.form-field-update-read').dblclick(function (e) {
    let formFieldUpdateRead = $(e.target);
    let formFieldUpdateEl = $(e.target).next('.form-field-update');
    formFieldUpdateRead.hide();
    formFieldUpdateEl.show();
    let formFiledUpdateInputEl = formFieldUpdateEl.children('.form-control');
    formFiledUpdateInputEl.focus();
    $('.form-field-update-cancel').click(function (e) {
        formFieldUpdateRead.show();
        formFieldUpdateEl.hide();
    });
});

$('.duration-picker').durationPicker({
    showDays: false,

    // callback function that triggers every time duration is changed
    //   value - duration in seconds
    //   isInitializing - bool value
    onChanged: function (value, isInitializing) {

        // isInitializing will be `true` when the plugin is initialized for the
        // first time or when it's re-initialized by calling `setValue` method
        console.log(value, isInitializing);
    }
});

$('select').selectpicker();
$('input[type=datetime-local]').datetimepicker();
$('.datetimepicker-input').each(function() {
    $(this).datetimepicker({});
});

$('#datetimepicker-start').datetimepicker();
$('#datetimepicker-end').datetimepicker({
    useCurrent: false
});
$("#datetimepicker-start").on("change.datetimepicker", function (e) {
    $('#datetimepicker-end').datetimepicker('minDate', e.date);
});
$("#datetimepicker-end").on("change.datetimepicker", function (e) {
    $('#datetimepicker-start').datetimepicker('maxDate', e.date);
});

$(".emoji").each(function(){
    $(this).html(emojione.shortnameToImage($(this).html()));
});

$(".full-value").hide();
$(".truncated-value").show();
$(".toggle-full-truncated-value").on('click', function(e) {
  e.preventDefault();
  $(e.target).prev('.truncated-value').slideToggle();
  $(e.target).next('.full-value').slideToggle();
})

function autosize(el) {
  setTimeout(function () {
    el.style.cssText = 'height:auto; padding:0';
    el.style.cssText = 'height:' + (el.scrollHeight + 30) + 'px';
  }, 0);
}

$('textarea').on('keydown', function() {autosize(this)});
