def init_security(app):
    from flask import render_template
    from flask_security import Security, SQLAlchemyUserDatastore, login_required, logout_user
    from shipka.persistance.models.auth import User, Role
    from shipka.webapp.forms import ExtendedRegisterForm
    from shipka.persistance import db
    security = Security()

    user_datastore = SQLAlchemyUserDatastore(db.sa_db, User, Role)
    security.init_app(app, user_datastore, register_form=ExtendedRegisterForm)

    @login_required
    @app.route("/log_out")
    def logout():
        logout_user()
        return render_template("logout.html")


def init_cached_login_mgr(app, cache):
    from flask_sqlalchemy_cache import FromCache
    from shipka.persistance.models.auth import User

    @app.login_manager.user_loader
    def _load_user(id=None):
        return User.query.options(FromCache(cache)).filter_by(id=id).first()
