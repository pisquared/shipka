import json
import re
from collections import defaultdict

from flask import current_app as app, jsonify, send_from_directory, g, url_for
from flask import flash, redirect
from flask import render_template, request
from flask_babel import refresh as refresh_lang
from flask_security import login_required

from shipka.persistance import db
from shipka.persistance.models.app import UserView
from shipka.persistance.models.auth import get_user, Notification
from shipka.persistance.models.models import Model
from shipka.persistance.models.tasks import Task
from shipka.workers import worker


def build_local_ctx():
    ctx = {}
    ctx['app_name'] = ''
    ctx['title'] = ''
    ctx['search_models'] = ''
    ctx['user'] = get_user()
    ctx['q'] = request.args.get('q', '')
    ctx['user_views'] = UserView.get_all()
    ctx['user_notifications'] = Notification.get_all()
    return ctx


HOOK_USER_VIEWS = defaultdict(list)
COMPONENTS_REGISTRY = dict()


class RequirementType(object):
    ModelInstance = "ModelInstance"

    @classmethod
    def from_request_args(cls):
        return request.args


def component_requirement(type, model_name=None, instance_uuid_from=None):
    class ComponentRequirement(object):
        def __init__(self, type, model_name=None, instance_uuid_from=None):
            self.ctx = dict()
            if type == RequirementType.ModelInstance:
                self.model_name = model_name
                self.instance_uuid_from = instance_uuid_from
                self.fulfil_method = self.get_model_instance
            else:
                self.fulfil_method = lambda ctx: ctx.update(self.ctx)

        def get_model_instance(self, ctx):
            instance_uuid = self.instance_uuid_from().get('instance_uuid')
            if not instance_uuid:
                flash('instance_uuid param required {}'.format(instance_uuid))
                # TODO: raise error
            model = Model.query(model_name=self.model_name,
                                instance_uuid=instance_uuid)
            if not model.instances or len(model.instances) > 1:
                flash('No {} with uuid {}'.format(self.model_name, instance_uuid))
                # TODO: raise error
            model_instance = model.instances.get(0)
            self.ctx['{}_instance'.format(self.model_name)] = model_instance
            self.ctx['{}_model'.format(self.model_name)] = model.model
            ctx.update(self.ctx)
            return ctx

        def fulfil(self, ctx):
            self.ctx = ctx
            return self.fulfil_method(ctx)

    def decorator(f):
        def wrap(ctx, *args, **kwargs):
            requirement = ComponentRequirement(type, model_name, instance_uuid_from)
            ctx = requirement.fulfil(ctx)
            return f(ctx)

        return wrap

    return decorator


def hook_user_view(url_name_match):
    def wrap(f):
        HOOK_USER_VIEWS[url_name_match].append(f)

    return wrap


def register_component(component_name):
    def wrap(f):
        COMPONENTS_REGISTRY[component_name] = f

    return wrap


@app.route('/views/<url_name>')
@login_required
def get_user_view(url_name):
    ctx = build_local_ctx()
    user_view = UserView.get_one_by(user=ctx.get('user'), url_name=url_name)
    if not user_view:
        flash('Error getting user_view "{}"'.format(url_name))
        return redirect('/')

    ctx['current_user_view'] = user_view
    ctx['components'] = user_view.components

    component_renders = dict()
    view_scripts = list()
    for component in user_view.components:
        component_name = component.name
        component_f = COMPONENTS_REGISTRY.get(component_name)
        component_render, component_scripts = component_f(ctx)
        component_renders[component_name] = component_render
        view_scripts += component_scripts
    ctx['component_renders'] = component_renders
    ctx['view_scripts'] = view_scripts

    matching_funcs = []
    for glob in HOOK_USER_VIEWS.keys():
        try:
            match = re.match(glob, url_name)
        except:
            match = None
        if match:
            matching_funcs += HOOK_USER_VIEWS[glob]

    for func in matching_funcs:
        ctx = func(ctx)

    return render_template('view.html',
                           ctx=ctx,
                           **ctx)


@app.route('/views/models')
@login_required
def get_view_models():
    ctx = build_local_ctx()
    ctx['models'] = Model.get_all()
    return render_template('view_models.html',
                           **ctx)


@app.route('/views/models/<model_name>')
@login_required
def get_view_model_many(model_name):
    ctx = build_local_ctx()
    ctx['model_instances'] = Model.query(model_name)
    return render_template('view_model_many.html',
                           **ctx)


@app.route('/views/models/<model_name>/<instance_uuid>')
@login_required
def get_view_model_single(model_name, instance_uuid):
    ctx = build_local_ctx()
    ctx['model_instances'] = Model.query(model_name, instance_uuid)
    return render_template('view_model_single.html',
                           **ctx)


@app.route('/views/models/<model_name>/new')
@login_required
def get_view_model_new(model_name):
    ctx = build_local_ctx()
    ctx['model'] = Model.serialize_model_def(model_name)
    return render_template('view_model_new.html',
                           **ctx)


@app.route('/views/models/<model_name>/<instance_uuid>/edit')
@login_required
def get_view_model_edit(model_name, instance_uuid):
    ctx = build_local_ctx()
    ctx['model_instances'] = Model.query(model_name, instance_uuid)
    return render_template('view_model_edit.html',
                           **ctx)


@app.route('/views/models/<model_name>/<instance_uuid>/delete_confirm')
@login_required
def get_view_model_delete_confirm(model_name, instance_uuid):
    ctx = build_local_ctx()
    ctx['model_instances'] = Model.query(model_name, instance_uuid)
    return render_template('view_model_delete_confirm.html',
                           **ctx)


@app.route("/search")
@login_required
def search():
    ctx = build_local_ctx()
    models = request.args.get("models", "").split(",")
    q = request.args.get("q")
    ctx['search_results'] = {}
    for model in models:
        if not model:
            continue
        ctx['search_results'][model] = Model.search(model, q)
    return render_template('search_results.html',
                           **ctx)


@app.route('/tasks/<task_name>/start', methods=['POST'])
def start_task(task_name):
    task_kwargs = {k: v for k, v in request.form.items() if k != 'csrf_token'}
    task_kwargs['logged_user_id'] = str(get_user().id)
    async_task = worker.send_task(task_name, [], task_kwargs)
    Task.create(name=task_name,
                uuid=async_task.id,
                kwargs=json.dumps(task_kwargs),
                status=async_task.status,
                )
    db.push()
    return redirect(url_for('get_task_status', task_id=async_task.id))


@app.route('/tasks/<task_id>/status')
def get_task_status(task_id):
    ctx = build_local_ctx()
    async_task = worker.AsyncResult(id=task_id)
    task = Task.get_one_by(uuid=async_task.id)
    ctx['async_task'] = {
        'result': async_task.result,
        'status': async_task.status,
    }
    ctx['task'] = task.serialize_flat()
    return render_template('task.html', **ctx)


@app.route('/tasks/<task_id>')
def get_async_task_result(task_id):
    result = worker.AsyncResult(id=task_id)
    return jsonify({
        "status": result.status,
        "result": result.result
    })


@app.route('/models/<model_name>/<action>', methods=["POST"])
@login_required
def model_action(model_name, action):
    if request.method == "POST":
        # TODO: Model action
        return redirect(request.form.get('_next') or request.referrer)


@app.route('/models/<model_name>/instances/<action>', methods=["POST"])
@app.route('/models/<model_name>/instances/<instance_uuid>/<action>', methods=["POST"])
@login_required
def model_instance_action(model_name, action, instance_uuid=None):
    if request.method == "POST":
        payload = request.form or request.json
        updates = {k: v for k, v in payload.items() if k != 'csrf_token' and not k.startswith('_')}
        fields = Model.dict_to_fields(model_name, updates)
        if action == 'create':
            for pre_create_hook in Model.get_pre_create_hooks(model_name):
                fields = pre_create_hook(fields)
            model_instance = Model.create_instance(model_name, fields=fields)
            for post_create_hook in Model.get_post_create_hooks(model_name):
                post_create_hook(model_instance)
        elif action == 'update':
            Model.update(model_name, updates=fields, instance_uuid=instance_uuid)
        elif action == 'delete':
            Model.delete_instance(model_name, instance_uuid)
        else:
            Model.do_action(model_name, action, instance_uuid=instance_uuid, fields=fields)
        return redirect(request.form.get('_next') or request.referrer)


@app.route("/media/<path:filepath>")
@login_required
def get_media(filepath):
    return send_from_directory(app.config.get("MEDIA_PATH"), filepath)


@app.route('/change_locale')
def change_locale():
    user = get_user()
    user.timezone = request.args.get('timezone')
    user.locale = request.args.get('locale')
    db.add(user)
    db.push()
    refresh_lang()
    return redirect(request.referrer)


@app.route('/service-worker.js')
def get_service_worker_js():
    return app.send_static_file('{}/js/service-worker.js'.format(g.env))


@app.route("/kitchensink")
@login_required
def get_kitchensink():
    ctx = build_local_ctx()
    return render_template("kitchensink.html", **ctx)


@app.route('/kitchensink-offline')
def get_offline():
    ctx = build_local_ctx()
    flash("You are offline!")
    return render_template('kitchensink.html',
                           **ctx)
