import importlib
import logging
import os

import jinja2
from flask import Flask, g, request

from populate import populate
from shipka.persistance import init_db, init_search, search_reindex_cron
from shipka.webapp.auth import init_security
from shipka.webapp.socketio import init_socketio
from shipka.webapp.util import create_jinja_helpers
from shipka.webapp.web import init_cache, init_limiter, init_gravatar, init_csrf

app = Flask(__name__,
            template_folder=os.path.join(os.environ.get("SHIPKA_PATH", '../../shipka'), 'webapp', 'templates'))


def configure(app):
    _basepath = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..'))

    environment = os.environ.get("WEBAPP_ENV", "prod")
    if environment == 'dev':
        logging.basicConfig(level=logging.INFO)
    app.config['WEBAPP_ENV'] = environment
    app.config['DEBUG'] = True if environment == "dev" else False
    app.config['HOST'] = os.environ.get("WEBAPP_HOST", "127.0.0.1")
    app.config['PORT'] = int(os.environ.get("WEBAPP_PORT", "5000"))

    app.config['BASEPATH'] = _basepath
    app.config['SECRETS_PATH'] = os.path.join(app.config.get("BASEPATH"), 'etc', 'secrets')
    app.config['SECRET_KEY'] = os.environ.get("SECRET_KEY", "not_secure")

    app.config['SECURITY_PASSWORD_SALT'] = os.environ.get("SECURITY_PASSWORD_SALT", "not_secure")
    app.config['SECURITY_PASSWORD_HASH'] = "bcrypt"
    app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
    app.config['SECURITY_CONFIRMABLE'] = False
    app.config['SECURITY_REGISTERABLE'] = True
    app.config['SECURITY_RECOVERABLE'] = False
    app.config['SECURITY_CHANGEABLE'] = True
    app.config['SECURITY_SEND_PASSWORD_CHANGE_EMAIL'] = False

    app.config['SECURITY_CHANGE_PASSWORD_TEMPLATE'] = 'override_security_templates/change_password.html',
    app.config['SECURITY_FORGOT_PASSWORD_TEMPLATE'] = 'override_security_templates/forgot_password.html',
    app.config['SECURITY_LOGIN_USER_TEMPLATE'] = 'override_security_templates/login_user.html'
    app.config['SECURITY_REGISTER_USER_TEMPLATE'] = 'override_security_templates/register_user.html',
    # app.config['SECURITY_RESET_PASSWORD_TEMPLATE'] = 'override_security_templates/reset_password.html',
    app.config['SECURITY_SEND_CONFIRMATION_TEMPLATE'] = 'override_security_templates/send_confirmation.html',
    # app.config['SECURITY_SEND_LOGIN_TEMPLATE'] = 'override_security_templates/send_login.html',

    app.config['DB_PATH'] = os.path.join(_basepath, 'database', 'app.db')
    app.config['MEDIA_PATH'] = os.path.join(_basepath, 'media')
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(app.config['DB_PATH'])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    app.config['QUEUE_ENABLED'] = True
    app.config['CELERY_RESULT_BACKEND'] = 'db+{}'.format(app.config['SQLALCHEMY_DATABASE_URI'])
    app.config['CELERY_BROKER_URL'] = os.environ.get("CELERY_BROKER_URL", 'amqp://guest:guest@localhost:5672/')
    app.config['task_track_started'] = True

    app.config['WHOOSHEE_DIR'] = os.path.join(_basepath, 'search_index')
    app.config['SEARCH_REINDEX_INTERVAL_SECONDS'] = 10

    app.config['SERVICE_WORKERS_ENABLED'] = False

    app.config['WEBSOCKETS_ENABLED'] = False
    app.config['CREATE_ANONYMOUS_USER'] = False

    app.config['WEBAPP_DEBUG_ENABLED'] = False
    app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
    app.config['DEBUG_TB_PROFILER_ENABLED'] = False

    # TODO: Bug, leads to stack overflow
    app.config['TRANSLATIONS_ENABLED'] = True
    app.config['TRANSLATION_LANGUAGES'] = ['en', 'de', 'bg']
    app.config['BABEL_TRANSLATION_DIRECTORIES'] = os.path.join(_basepath, 'translations')
    app.config['BABEL_DEFAULT_TIMEZONE'] = 'UTC'
    app.config['BABEL_DEFAULT_LOCALE'] = 'en'

    app.config['MAX_TEXT_LENGTH'] = 300


def init_web(app):
    @app.before_request
    def inject_env():
        g.env = app.config.get("WEBAPP_ENV")


def init_blueprints(app):
    with app.app_context():
        from shipka.webapp import views
        if socketio:
            from shipka.webapp import views_websocket
    for _, dirs, _ in os.walk('webapp'):
        for dir in dirs:
            if not dir.startswith('bp_'):
                continue
            with app.app_context():
                module = importlib.import_module("webapp.{}".format(dir))
            attr = getattr(module, dir)
            app.register_blueprint(attr)
        break
    for _, dirs, _ in os.walk('apps'):
        for dir in dirs:
            skip = False
            for not_dir in ['__pycache__']:
                if dir.startswith(not_dir):
                    skip = True
                    break
            if skip:
                continue
            with app.app_context():
                module = importlib.import_module("apps.{}".format(dir))
            attr = getattr(module, dir)
            app.register_blueprint(attr)
        break


def _init_debug_toolbar(app):
    from flask_debugtoolbar import DebugToolbarExtension
    debug_toolbar = DebugToolbarExtension()
    debug_toolbar.init_app(app)


def _init_translations(app):
    from flask_babelex import Babel
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        from shipka.persistance.models.auth import get_user
        # if a user is logged in, use the locale from the user settings
        user = get_user(create=False)
        if user is not None:
            return user.locale
        # otherwise try to guess the language from the user accept
        # header the browser transmits
        return request.accept_languages.best_match(app.config.get('TRANSLATION_LANGUAGES', ['en']))

    @babel.timezoneselector
    def get_timezone():
        from shipka.persistance.models.auth import get_user
        user = get_user()
        if user is not None:
            return user.timezone


socketio = None


def create_app():
    configure(app)
    init_web(app)
    init_db(app)
    init_security(app)
    init_cache(app)
    init_limiter(app)
    init_gravatar(app)
    init_csrf(app)
    global socketio
    socketio = init_socketio(app)
    init_blueprints(app)
    create_jinja_helpers(app)
    if app.config.get('WEBAPP_DEBUG_ENABLED') and app.config.get('DEBUG'):
        _init_debug_toolbar(app)
    if app.config.get("TRANSLATIONS_ENABLED"):
        _init_translations(app)
    init_search(app)
    return app, socketio


def reindex_search():
    configure(app)
    init_db(app)
    init_search(app)
    with app.app_context():
        search_reindex_cron(app)


def _run_socketio(app, socketio):
    if app.config.get('SSL_ENABLED'):
        # TODO: ssl
        socketio.run(app,
                     host=app.config.get('HOST'),
                     port=app.config.get('PORT'),
                     log_output=True,
                     keyfile='provisioning/dev_cert/localhost.key',
                     certfile='provisioning/dev_cert/localhost.crt', )
    else:
        socketio.run(app,
                     host=app.config.get('HOST'),
                     port=app.config.get('PORT'),
                     log_output=True)


def main():
    app, socketio = create_app()
    populate(app)
    if app.config.get('WEBSOCKETS_ENABLED'):
        _run_socketio(app, socketio)
    else:
        app.run(host=app.config.get('HOST'),
                port=app.config.get('PORT'))
