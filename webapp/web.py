from shipka.webapp.auth import init_cached_login_mgr


def init_cache(app):
    from flask_cache import Cache
    cache = Cache()
    if app.config.get("WEBAPP_ENV") == 'prod':
        cache.init_app(app)
        init_cached_login_mgr(app, cache)
    return cache


def init_gravatar(app):
    global gravatar
    from flask_gravatar import Gravatar
    gravatar = Gravatar(app)


def init_limiter(app):
    global limiter
    from flask_limiter import Limiter
    from flask_limiter.util import get_remote_address
    limiter = Limiter(app, key_func=get_remote_address)


def init_csrf(app):
    from flask_wtf import CSRFProtect
    CSRFProtect(app)
