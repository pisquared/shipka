import datetime
import json
from collections import defaultdict

from flask import request, jsonify, flash, redirect
from flask_login import login_required
from flask_security import roles_required
from inflection import pluralize
from wtforms import TextAreaField
from wtforms.fields.core import UnboundField, StringField, BooleanField, SelectField
from wtforms.fields.html5 import DateTimeField, IntegerField
from wtforms_alchemy import ModelForm

from shipka.persistance.models.auth import get_user
from shipka.webapp.util import get_now_user, serialize_request_form


def json_serialize(d):
    rv = {}
    for field, value in d.iteritems():
        try:
            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y-%m-%d %H:%M:%S%Z")
            json.dumps(value)
            rv[field] = value
        except TypeError:
            continue
    return rv


class ModelView(object):
    def __init__(self,
                 name=None,
                 model=None,
                 form=None,
                 form_include=None,
                 form_exclude=None,
                 form_only=None,
                 retrievable_login_required=True,
                 retrievable_roles_required=None,
                 creatable=True,
                 creatable_login_required=True,
                 creatable_roles_required=None,
                 editable=True,
                 editable_login_required=True,
                 editable_roles_required=None,
                 deletable=True,
                 deletable_login_required=True,
                 deletable_roles_required=None,
                 user_needed=True,
                 ):
        form_include = form_include or []
        form_exclude = form_exclude or []
        form_only = form_only or []
        retrievable_roles_required = retrievable_roles_required or []
        creatable_roles_required = creatable_roles_required or []
        editable_roles_required = editable_roles_required or []
        deletable_roles_required = deletable_roles_required or []

        self.name = name
        self.name_human = name.replace('_', ' ')
        self.model = model
        self.model_constants = self._get_model_constants()
        self.form = form
        if not self.form:
            for ef in ['created_ts', 'updated_ts', 'deleted_ts', 'deleted']:
                if hasattr(self.model, ef):
                    form_exclude += [ef]
            self.form = self._create_form(form_include, form_exclude, form_only)
        self.form_include = self._get_form_include()
        self.form_include_map = self._get_form_include_map()
        self.name_plural = pluralize(self.name)

        self.retrievable_login_required = retrievable_login_required
        self.retrievable_roles_required = retrievable_roles_required

        self.creatable = creatable
        self.creatable_login_required = creatable_login_required
        self.creatable_roles_required = creatable_roles_required

        self.editable = editable
        self.editable_login_required = editable_login_required
        self.editable_roles_required = editable_roles_required

        self.deletable = deletable
        self.deletable_login_required = deletable_login_required
        self.deletable_roles_required = deletable_roles_required

        self.user_needed = user_needed

    def _create_form(self, form_include, form_exclude, form_only):
        """
        By default WTForms-Alchemy excludes a column from the ModelForm
        if one of the following conditions is True:
            Column is primary key
            Column is foreign key
            Column is DateTime field which has default value
                (usually this is a generated value)
            Column is of TSVectorType type
            Column is set as model inheritance discriminator field
        """

        return type('DynamicForm__{}'.format(self.name), (ModelForm,), {
            'Meta': type('Meta', (), {
                'model': self.model,
                'include': form_include,
                'exclude': form_exclude,
                'only': form_only,
            })
        })

    def _get_model_constants(self):
        return {c: getattr(self.model, c) for c in dir(self.model) if c.isupper()}

    def _get_form_include(self):
        form_include = [f for f in dir(self.form) if type(getattr(self.form, f)) is UnboundField]
        return ['request_sid'] + form_include

    def _get_form_include_map(self):
        """Maps fields to str representative type of the column"""
        rv = {}
        for f in dir(self.form):
            attr = getattr(self.form, f)
            if type(attr) is UnboundField:
                field_class = attr.field_class
                if issubclass(field_class, StringField):
                    rv[f] = {'type': 'str'}
                elif issubclass(field_class, DateTimeField):
                    rv[f] = {'type': 'dt'}
                elif issubclass(field_class, IntegerField):
                    rv[f] = {'type': 'int'}
                    if f.endswith('_id'):
                        connected_model = f.split('_id')[0]
                        rv[f]['type'] = 'select_model'
                        rv[f]['model'] = connected_model
                elif issubclass(field_class, BooleanField):
                    rv[f] = {'type': 'bool'}
                elif issubclass(field_class, TextAreaField):
                    rv[f] = {'type': 'str_long'}
                elif issubclass(field_class, SelectField):
                    rv[f] = {'type': 'select'}
                    rv[f]['choices'] = attr.kwargs['choices']
                else:
                    rv[f] = {'type': 'unknown', 'field_class': str(field_class)}
                rv[f]['default'] = attr.kwargs['default']
        return rv

    def init_form(self, **kwargs):
        user = get_user()
        form_instance = self.form(**kwargs)
        if hasattr(self.form, 'choice_methods'):
            for choice_field, meth in self.form.choice_methods.iteritems():
                if type(meth) is str and meth.startswith('user.'):
                    choices = [(a.id, a.name) for a in getattr(user, meth.split('user.')[1])]
                else:
                    choices = meth
                getattr(form_instance, choice_field).choices = choices
        return form_instance

    def add_create(self, form_data):
        from shipka.store import database
        from shipka.webapp import socketio
        user = get_user()
        now = get_now_user(user)
        latest_cud = self.LatestCUD.get_or_create(user_id=user.id, model_name=self.name)
        latest_cud.created_ts = get_now_user(user)
        request_sid = form_data.pop('request_sid', None)
        instance = self.model.create(created_ts=now, user=user, **form_data)
        database.add(instance)
        database.push()
        rv = {'instance': instance.serialize_flat(), 'request_sid': request_sid,
              'status': 'success'}
        if socketio:
            socketio.emit('{name}.created'.format(name=self.name), rv, room=user.id)
        return rv

    def add_update(self, instance, updated_elements):
        from shipka.store import database
        from shipka.webapp import socketio
        user = get_user()
        now = get_now_user(user)
        latest_cud = self.LatestCUD.get_or_create(user_id=user.id, model_name=self.name)
        latest_cud.updated_ts = now
        request_sid = updated_elements.pop('request_sid', None)
        print('updated_elements', updated_elements)
        database.update(instance, user_id=user.id, updated_ts=now, **updated_elements)
        database.push()
        rv = {'id': instance.id, 'updates': json_serialize(updated_elements),
              'status': 'success', 'request_sid': request_sid}
        if socketio:
            socketio.emit('{name}.updated'.format(name=self.name), rv, room=user.id)
        return rv

    def add_delete(self, instance):
        from shipka.store import database
        from shipka.webapp import socketio
        user = get_user()
        instance_id = instance.id
        latest_cud = self.LatestCUD.get_or_create(user_id=user.id, model_name=self.name)
        latest_cud.deleted_ts = get_now_user(user)
        database.delete(instance)
        database.push()
        request_sid = request.form.get('request_sid', None)
        rv = {'id': instance_id, 'request_sid': request_sid, 'status': 'success'}
        if socketio:
            socketio.emit('{name}.deleted'.format(name=self.name), rv, room=user.id)
        return rv

    def before_create(self, form_data):
        """override if needed - need to return form_data"""
        return form_data

    def after_create(self, serialized_instance):
        """override if needed - need to return serialized_instance"""
        return serialized_instance

    def handle_patch(self, instance, json):
        """override if needed - need to return updated elements"""
        request_sid = json.pop('request_sid', None)
        return {'request_sid': request_sid}

    def handle_get_filters(self, filters):
        """override if needed - need to filtered elements"""
        return self.default_get_all()

    def default_filters(self):
        filters = []
        if not self.user_needed:
            return []
        if hasattr(self.model, 'user'):
            user = get_user()
            filters = filters + [self.model.user == user]
        return filters

    def additional_filters(self):
        """override if needed"""
        return []

    def default_get_all(self):
        filters = self.default_filters()
        filters = filters + self.additional_filters()
        all = self.model.get_all_by(filters=filters)
        return all

    def default_get_one(self, instance_id):
        """override if needed - get one element by id"""
        if not self.user_needed:
            return self.model.get_one_by(id=instance_id)
        user = get_user()
        return self.model.get_one_by(id=instance_id, user=user)

    def serialize_batch_create(self):
        """override if needed - to create a batch creatable entity"""
        return request.form

    def serialize_batch_update(self):
        instances_updates = defaultdict(dict)
        for id_name in request.form.iterkeys():
            if id_name in ['csrf_token']:
                continue
            values = request.form.getlist(id_name)
            for value in values:
                splitted = id_name.split('_')
                instance_id, name = splitted[0], '_'.join(splitted[1:])
                if name.endswith('_bool') and name in instances_updates[instance_id]:
                    # In this case, the boolean flag is already in the custom form.
                    # That means that the second one is either the True of False but
                    # that means it is True if we are sending both (inputs checkboxes
                    # send only on checked=checked.
                    instances_updates[instance_id][name] = "1"
                    continue
                instances_updates[instance_id][name] = value
        return instances_updates

    def retrieve(self):
        with_ = request.args.getlist('with')
        if request.args:
            instances = self.handle_get_filters(request.args)
        else:
            instances = self.default_get_all()
        return jsonify([instance.serialize_flat(with_=with_) for instance in instances])

    def retrieve_single(self, instance_id):
        with_ = request.args.getlist('with')
        instance = self.default_get_one(instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error}), 404
        return jsonify(instance.serialize_flat(with_=with_))

    def create(self):
        payload = request.form or request.json
        include = self.form_include
        form_data = serialize_request_form(include, payload)
        form_instance = self.init_form(**form_data)
        if not form_instance.validate():
            return {"Error": form_instance.errors, 'status': 'error'}, 400
        try:
            form_data = self.before_create(form_data)
        except Exception as e:
            return {"Error": e.message, 'status': 'error'}, 400
        rv = self.add_create(form_data)
        rv = self.after_create(rv)
        return rv, 200

    def create_api(self):
        rv, status_code = self.create()
        return jsonify(rv), status_code

    def create_nojs(self):
        rv, _ = self.create()
        status = rv.get('status')
        next = request.form.get('next') or request.referrer
        if status == 'success':
            flash("Sucessfully created {}".format(self.name), 'success')
        elif status == 'error':
            flash("Error creating {}".format(self.name))
        return redirect(next)

    def after_batch_create(self, responses):
        """Override this for custom behaviour"""
        return responses

    def batch_create(self):
        rvs = []
        include = self.form_include
        custom_forms = self.serialize_batch_create()
        for custom_form in custom_forms:
            form_data = serialize_request_form(include, custom_form)
            form_instance = self.init_form(**form_data)
            if not form_instance.validate():
                return jsonify({"Error": form_instance.errors, 'status': 'error'}), 400
            try:
                form_data = self.before_create(form_data)
            except Exception as e:
                return jsonify({"Error": e.message, 'status': 'error'}), 400
            rvs.append(self.add_create(form_data))
        rvs = self.after_batch_create(rvs)
        return jsonify(rvs)

    def put(self, instance_id):
        """Used to update the whole element"""
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return {"Error": error, 'status': 'error'}, 404
        payload = request.form or request.json
        include = self.form_include
        form_data = serialize_request_form(include, payload)
        form_instance = self.init_form(**form_data)
        if not form_instance.validate():
            return jsonify({"Error": form_instance.errors, 'status': 'error'}), 400
        rv = self.add_update(instance, form_data)
        return rv, 202

    def put_api(self, instance_id):
        rv, status_code = self.put(instance_id)
        return jsonify(rv), status_code

    def update_nojs(self, instance_id):
        rv, _ = self.put(instance_id)
        status = rv.get('status')
        next = request.form.get('next') or request.referrer
        if status == 'success':
            flash("Sucessfully updated {}".format(self.name), 'success')
        elif status == 'error':
            flash("Error updating {}".format(self.name))
        return redirect(next)

    def batch_update(self):
        rvs = []
        include = self.form_include
        instances_updates = self.serialize_batch_update()
        for instance_id, custom_form in instances_updates.iteritems():
            instance = self.model.get_one_by(id=instance_id)
            form_data = serialize_request_form(include, custom_form)
            form_instance = self.init_form(**form_data)
            if not form_instance.validate():
                return jsonify({"Error": form_instance.errors, 'status': 'error'}), 400
            rvs.append(self.add_update(instance, form_data))
        return jsonify(rvs)

    def patch(self, instance_id):
        """Used to (usually) update just a single item"""
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error, 'status': 'error'}), 404
        payload = request.form or request.json
        try:
            updated_elements = self.handle_patch(instance, payload)
        except Exception as e:
            return jsonify({"Error": e.message}), 400
        rv = self.add_update(instance, updated_elements)
        return jsonify(rv)

    def delete(self, instance_id):
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return {"Error": error, 'status': 'error'}, 404
        rv = self.add_delete(instance)
        return rv, 202

    def delete_api(self, instance_id):
        rv, status_code = self.delete(instance_id)
        return jsonify(rv), status_code

    def delete_nojs(self, instance_id):
        rv, _ = self.delete(instance_id)
        status = rv.get('status')
        next = request.form.get('next') or request.referrer
        if status == 'success':
            flash("Sucessfully deleted {}".format(self.name), 'success')
        elif status == 'error':
            flash("Error deleting {}".format(self.name))
        return redirect(next)

    def _register(self):
        """Override this to register more views"""

    def decorate_view_func(self, view_func, is_login, are_roles):
        if is_login:
            view_func = login_required(view_func)
        if are_roles:
            view_func = roles_required(*are_roles)(view_func)
        return view_func

    def _def_create_routes(self, blueprint):
        creatable_view_func = self.create_api
        creatable_view_func = self.decorate_view_func(creatable_view_func,
                                                      self.creatable_login_required,
                                                      self.creatable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}'.format(name_plural=self.name_plural),
                               endpoint='{name}_create'.format(name=self.name),
                               view_func=creatable_view_func,
                               methods=['POST'])
        creatable_view_func_no_js = self.create_nojs
        creatable_view_func_no_js = self.decorate_view_func(creatable_view_func_no_js,
                                                            self.creatable_login_required,
                                                            self.creatable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/create'.format(name_plural=self.name_plural),
                               endpoint='{name}_create_nojs'.format(name=self.name),
                               view_func=creatable_view_func_no_js,
                               methods=['POST'])
        batch_create_view_func = self.batch_create
        batch_create_view_func = self.decorate_view_func(batch_create_view_func,
                                                         self.creatable_login_required,
                                                         self.creatable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/batch_create'.format(name_plural=self.name_plural),
                               endpoint='{name}_batch_create'.format(name=self.name),
                               view_func=batch_create_view_func,
                               methods=["POST"])

    def _def_retrieve_routes(self, blueprint):
        retrieve_view_func = self.retrieve
        retrieve_view_func = self.decorate_view_func(retrieve_view_func,
                                                     self.retrievable_login_required,
                                                     self.retrievable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}'.format(name_plural=self.name_plural),
                               endpoint='{name}_retrieve_all'.format(name=self.name),
                               view_func=retrieve_view_func,
                               methods=['GET'])
        retrieve_single_view_func = self.retrieve_single
        retrieve_single_view_func = self.decorate_view_func(retrieve_single_view_func,
                                                            self.retrievable_login_required,
                                                            self.retrievable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_retrieve_single'.format(name=self.name),
                               view_func=retrieve_single_view_func,
                               methods=['GET'])

    def _def_update_routes(self, blueprint):
        patch_view_func = self.patch
        patch_view_func = self.decorate_view_func(patch_view_func,
                                                  self.editable_login_required,
                                                  self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_patch'.format(name=self.name),
                               view_func=patch_view_func,
                               methods=['PATCH'])
        put_api_view_func = self.put_api
        put_api_view_func = self.decorate_view_func(put_api_view_func,
                                                    self.editable_login_required,
                                                    self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_put'.format(name=self.name),
                               view_func=put_api_view_func,
                               methods=['PUT'])
        update_nojs_view_func = self.update_nojs
        update_nojs_view_func = self.decorate_view_func(update_nojs_view_func,
                                                        self.editable_login_required,
                                                        self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_update_nojs'.format(name=self.name),
                               view_func=update_nojs_view_func,
                               methods=['POST'])
        batch_update_view_func = self.batch_update
        batch_update_view_func = self.decorate_view_func(batch_update_view_func,
                                                         self.editable_login_required,
                                                         self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/batch_update'.format(name_plural=self.name_plural),
                               endpoint='{name}_batch_update'.format(name=self.name),
                               view_func=batch_update_view_func,
                               methods=["POST"])

    def _def_delete_routes(self, blueprint):
        delete_api_view_func = self.delete_api
        delete_api_view_func = self.decorate_view_func(delete_api_view_func,
                                                       self.deletable_login_required,
                                                       self.deletable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_delete'.format(name=self.name),
                               view_func=delete_api_view_func,
                               methods=['DELETE'])
        delete_nojs_view_func = self.delete_nojs
        delete_nojs_view_func = self.decorate_view_func(delete_nojs_view_func,
                                                        self.deletable_login_required,
                                                        self.deletable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>/delete'.format(name_plural=self.name_plural),
                               endpoint='{name}_delete_nojs'.format(name=self.name),
                               view_func=delete_nojs_view_func,
                               methods=['GET', 'POST'])

    def register(self, blueprint):
        self._def_retrieve_routes(blueprint)
        if self.creatable:
            self._def_create_routes(blueprint)
        if self.editable:
            self._def_update_routes(blueprint)
        if self.deletable:
            self._def_delete_routes(blueprint)
        self._register()
