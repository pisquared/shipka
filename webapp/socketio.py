def init_socketio(app):
    if app.config.get('WEBSOCKETS_ENABLED'):
        global socketio
        from flask_socketio import SocketIO
        socketio = SocketIO(app)
        return socketio
