from flask_security.forms import ConfirmRegisterForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, EqualTo


class ExtendedRegisterForm(ConfirmRegisterForm):
    name = StringField("Name", [DataRequired()])
    password_confirm = PasswordField("Retype Password",
                                     validators=[EqualTo("password", message="Passwords do not match.")])
