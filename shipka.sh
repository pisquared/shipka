#!/usr/bin/env bash

set -e
set -x

which jq || (sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get install -y jq)

if  [ -v $SHIPKA_PATH ]; then
  echo "You need to specify SHIPKA_PATH env variable"
  exit 1
fi

echo "shipka is at: $SHIPKA_PATH"

#!/usr/bin/env bash
HELP="
Usage $0 [ assets | admin_webapp | beat | project-create | celery | db | db_connect| eventlet | flower | journal | pgweb | populate | prod_bootstrap | prod_push | recreate | reprov_project | runserver | shell | ssh | ssh_tunnel | status | sys | test | test_prod | translate | vm ]

    DEV ACTIVITIES:
    ===============
    project-config      Config a new project
    project-create      Create a new project
    populate            Populates a database with initial values.
    recreate            Recreates everything - db, workers etc.
    reprov_project      Run the project specific provisioners only.
    test                Run BDD tests with lettuce
    test_prod           Setup a local prod machine for testing purposes
                          EXCEPT:
                             - setting up an SSL certificate
    translate           Generates translation files.

    ADMIN ACTIVITIES:
    ===============
    **!!Only if you have configured sensitive.py and have sensitive folder!!**
    prod_bootstrap      TODO Bootstrap Production - designed for bootstrapping before ansible can kick in. Works on Digital Ocean
    prod_push           Push to production:

    SERVICES:
    ===============
    sys                 alias to sudo systemctl #@
    journal             alias to sudo journalctl -u #@
    vm                  alias to vagrant #@
    runall              Run default development environment - server+worker+cron.
    runserver           Runs the server.
        --debug-fe=True      Enable debugging of front end by not compressing of static files.
        --debug-tb=True      Enable debug toolbar.
        --ssl=True           Enable running with certificates.
        prod                 Run in production.
    eventlet            Run eventlet server.
    celery              Run celery worker.
    beat                Run beat scheduler.
    db_connect          connect to the database
    ssh                 alias to vagrant ssh #@
    ssh_tunnel          creates a tunnel to all services

    ADMIN SERVICES:
    ===============
    admin_webapp        Run admin webapp.
    flower              Run flower management.
    pgweb               Run database management.

    MANAGEMENT:
    ===============
    assets              Manage static assets.
    db                  Perform database migrations.

    DEBUGING:
    ===============
    status              View the status of the installed and running services
    shell               Runs a Python shell inside Flask application context.
"

HELP_TRANSLATION="
USAGE ./manage.sh translate [extract|gen {lang}|compile|update]

    extract             Extract strings in files as defined in translations/babel.cfg
    gen {lang}          Init translations for {lang}
    compile             Compile all translations
    update              Use after a new extract - it may mark strings as fuzzy.
"

project_create() {
    shift
    PROJECT_PATH=$1
    if [[ -z "$PROJECT_PATH" ]]; then
      read -p "Enter project path: "  PROJECT_PATH
    fi
    if [[ -z "$PROJECT_PATH" ]]; then
        echo "ERROR: Specify project path"
        exit 1
    else
        echo "INFO: Project path: $PROJECT_PATH"
        PROJECT_PATH=`realpath $PROJECT_PATH`
        echo "INFO: Absolute project path: $PROJECT_PATH"
        if [[ "$PROJECT_PATH" == $SHIPKA_PATH* ]]; then
            echo "ERROR: Project path can't be inside this directory. Exiting..."
            exit 1
        fi
        if [ -d $PROJECT_PATH ]; then
            echo "ERROR: Project path exists. Please remove or specify another. Exiting..."
            exit 1
        else
            echo "INFO: Project path doesn't exist, creating..."
            mkdir -p $PROJECT_PATH
        fi
    fi
    PROJECT_NAME=$(basename $PROJECT_PATH)
    echo "INFO: Bootstrapping project $PROJECT_NAME..."
    cp ${SHIPKA_PATH}/templates/_.gitignore ${PROJECT_PATH}/.gitignore
    cp ${SHIPKA_PATH}/templates/requirements.txt ${PROJECT_PATH}/requirements.txt
    cp ${SHIPKA_PATH}/templates/run.py ${PROJECT_PATH}/run.py
    cp ${SHIPKA_PATH}/templates/populate.py ${PROJECT_PATH}/populate.py
    cp -r ${SHIPKA_PATH}/templates/workers ${PROJECT_PATH}/workers
    cp -r ${SHIPKA_PATH}/templates/etc ${PROJECT_PATH}/etc
    cp -r ${SHIPKA_PATH}/templates/webapp ${PROJECT_PATH}/webapp
    cp -r ${SHIPKA_PATH}/templates/apps ${PROJECT_PATH}/apps
    touch ${PROJECT_PATH}/deb_packages.txt

    echo "INFO: Create virtualenv $PROJECT_NAME..."
    virtualenv -p python3 $PROJECT_PATH/venv
    cd "$PROJECT_PATH" || exit 2
    git init .
    command_install

    echo "INFO: Completed bootstrapping project $PROJECT_NAME!"
    exec bash  # needed because cd changes dir only in the subshell
}

generate_requirements() {
  python ${SHIPKA_PATH}/shipka.py generate_requirements `pwd` > requirements.txt
}

command_install() {
  VENV_DIR=${VENV_DIR:-venv}
  echo "venv dir is: ${VENV_DIR}"

  which virtualenv || (sudo apt-get update && sudo DEBIAN_FRONTEND=noninteractive apt-get install -y python-virtualenv python3-pip)

  cat shipka_config.json | jq -r .deb_packages[] > deb_packages.txt
  < deb_packages.txt xargs sudo apt-get install -y

  cp -r ${SHIPKA_PATH}/templates/etc etc

  [[ -d "${VENV_DIR}" ]] || virtualenv -p python3 ${VENV_DIR}
  generate_requirements
  pip_install_requirements
  pip_freeze
  PYTHON_VERSION=$(python3 -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
  patch -p0 --forward "${VENV_DIR}/lib/python${PYTHON_VERSION}/site-packages/flask_cache/jinja2ext.py" < "${SHIPKA_PATH}/provision/flask_cache_jinja.patch" || true
  patch -p0 --forward "${VENV_DIR}/lib/python${PYTHON_VERSION}/site-packages/flask_sqlalchemy_cache/core.py" < "${SHIPKA_PATH}/provision/flask_sql_cache_core.patch" || true
  [[ -L shipka ]] || ln -s $SHIPKA_PATH
}

command_db() {
    DB_COMMAND=$2
    case "$DB_COMMAND" in
        migrate) "${SHIPKA_REMOTE_PATH}/scripts/db_migrate.sh" "$@"
        ;;
        *) ./_manage.py "$@"
        ;;
    esac
    return $?
}

command_translate() {
    TRANSLATE_COMMAND=$2
    shift
    shift
    case "$TRANSLATE_COMMAND" in
        extract) pybabel extract -F translations/babel.cfg -o translations/messages.pot .
        ;;
        gen) pybabel init -i translations/messages.pot -d translations -l "$@"
        ;;
        compile) pybabel compile -d translations
        ;;
        update) pybabel update -i translations/messages.pot -d translations
        ;;
        *)  >&2 echo -e "${HELP_TRANSLATION}"
        ;;
    esac
    return $?
}

command_ssh_tunnel() {
    APP_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["app_server_local_port"])'`
    DB_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["database_mgmt_port"])'`
    WRKS_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["workers_mgmt_port"])'`
    MONIT_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["monitoring_mgmt_port"])'`
    Q_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["queue_mgmt_port"])'`
    ERR_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["error_tracking_port"])'`
    ADMIN_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["admin_webapp_port"])'`
    echo "connecting to: $MONIT_MGMT_PORT:localhost:$MONIT_MGMT_PORT -L $APP_PORT:localhost:$APP_PORT -L $ADMIN_PORT:localhost:$ADMIN_PORT -L $DB_MGMT_PORT:localhost:$DB_MGMT_PORT -L $WRKS_MGMT_PORT:localhost:$WRKS_MGMT_PORT -L $Q_MGMT_PORT:localhost:$Q_MGMT_PORT -L $ERR_MGMT_PORT:localhost:$ERR_MGMT_PORT"
    vagrant ssh -- -N -L $MONIT_MGMT_PORT:localhost:$MONIT_MGMT_PORT -L $APP_PORT:localhost:$APP_PORT -L $ADMIN_PORT:localhost:$ADMIN_PORT -L $DB_MGMT_PORT:localhost:$DB_MGMT_PORT -L $WRKS_MGMT_PORT:localhost:$WRKS_MGMT_PORT -L $Q_MGMT_PORT:localhost:$Q_MGMT_PORT -L $ERR_MGMT_PORT:localhost:$ERR_MGMT_PORT
}

db_connect() {
    APP_NAME=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["app_name"])'`
    DB_USER=`python -c 'import json;j=json.load(open("etc/sensitive_config.json"));print(j["db_user"])'`
    psql -h localhost -p 5432 -U $DB_USER "${APP_NAME}_dev_db"
}

pip_freeze() {
  VENV_DIR=${VENV_DIR:-venv}
  source "${VENV_DIR}/bin/activate"
  pip freeze > requirements.txt
  sed -i '/pkg-resources==0.0.0/d' requirements.txt
}

pip_install() {
  VENV_DIR=${VENV_DIR:-venv}
  source "${VENV_DIR}/bin/activate"
  pip install $@
  pip_freeze
}

pip_install_requirements() {
  VENV_DIR=${VENV_DIR:-venv}
  source "${VENV_DIR}/bin/activate"
  sed -i '/pkg-resources==0.0.0/d' requirements.txt
  pip install -r requirements.txt
}


command_pip() {
    COMMAND=$1
    shift
    case "$COMMAND" in
        freeze) pip_freeze "$@"
        ;;
        install) pip_install "$@"
        ;;
        install_requirements) pip_install_requirements "$@"
        ;;
    esac
    return $?
}

command_run_dev() {
  VENV_DIR=${VENV_DIR:-venv}
  source "${VENV_DIR}/bin/activate"
  export WEBAPP_ENV=dev && python run.py
}

command_install_docker() {
  sudo apt-get update
  sudo apt-get install -y make apt-transport-https \
      ca-certificates \
      curl \
      software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-key fingerprint 0EBFCD88
  sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
  sudo apt-get update
  sudo apt-get -y install docker-ce
  sudo usermod -a -G docker $USER
}

command_project_config() {
  source "${SHIPKA_PATH}/venv/bin/activate"
  export WEBAPP_ENV=dev && python bootstrap/__init__.py &
  xdg-open "http://localhost:5000"
}

command_main() {
    INITIAL_COMMAND=$1
    case "$INITIAL_COMMAND" in
        assets|shell|runserver|runadmin|populate) ./_manage.py "$@"
        ;;
        admin_webapp) "${SHIPKA_REMOTE_PATH}/scripts/run_admin_webapp.sh" "$@"
        ;;
        runall) sudo systemctl stop celery beat && ./_manage.py "$@"
        ;;
        beat) "${SHIPKA_REMOTE_PATH}/scripts/run_beat.sh" "$@"
        ;;
        project-config) command_project_config  "$@"                                                 # verified with new shipka
        ;;
        project-create) project_create  "$@"                                                 # verified with new shipka
        ;;
        celery) "${SHIPKA_REMOTE_PATH}/scripts/run_celery.sh" "$@"
        ;;
        db) command_db "$@"
        ;;
        db_connect) db_connect
        ;;
        eventlet) "${SHIPKA_REMOTE_PATH}/scripts/run_eventlet.sh" "$@"
        ;;
        journal) shift; sudo journalctl -u "$@"
        ;;
        flower) "${SHIPKA_REMOTE_PATH}/scripts/run_flower.sh" "$@"
        ;;
        install) command_install "$@"                                                        # verified with new shipka
        ;;
        install-docker) command_install_docker "$@"                                          # verified with new shipka
        ;;
        pip) shift; command_pip "$@"                                                         # verified with new shipka
        ;;
        pgweb) "${SHIPKA_REMOTE_PATH}/scripts/run_pgweb.sh" "$@"
        ;;
        prod_bootstrap) "${SHIPKA_REMOTE_PATH}scripts/prod_bootstrap.sh" "$@"
        ;;
        prod_push) "${SHIPKA_REMOTE_PATH}/scripts/prod_push.sh" "$@"
        ;;
        recreate) "${SHIPKA_REMOTE_PATH}/scripts/recreate.sh"
        ;;
        reprov_project) vagrant provision --provision-with project_specific
        ;;
        run-dev) command_run_dev "$@"                                                        # verified with new shipka
        ;;
        status) shift && "${SHIPKA_REMOTE_PATH}/scripts/status.py" "$@"
        ;;
        ssh) shift; vagrant ssh "$@"
        ;;
        ssh_tunnel) shift; command_ssh_tunnel
        ;;
        sys) shift; sudo systemctl "$@"
        ;;
        test) lettuce
        ;;
        test_prod) "${SHIPKA_PATH}/scripts/test_prod.sh"
        ;;
        translate) command_translate "$@"
        ;;
        vm) shift; vagrant "$@"
        ;;
        *) >&2 echo -e "${HELP}"
        return 1
        ;;
    esac
    return $?
}

command_main "$@"
