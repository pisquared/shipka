from flask import Blueprint
from shipka.webapp import socketio

app = Blueprint('app', __name__,
                     template_folder='components')

from apps.app import components, models
