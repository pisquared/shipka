import json
import os

import yaml


def create_user(user_email, password, user_name, roles=None, app=None, user_apps=None):
    from flask_security.utils import hash_password
    from shipka.persistance import db
    from shipka.persistance.models.auth import User

    user = User.get_one_by(email=user_email)

    if not user:
        user = User.create(email=user_email,
                           name=user_name,
                           password=hash_password(password))
        if roles:
            user.roles = roles
        db.add(user)
        from shipka.persistance.models.auth import global_user
        with global_user(user):
            for user_app in user_apps:
                create_user_views(app, user_app, user)
                create_models(app, user_app)
    return user


def create_user_views(app, user_app, admin_user):
    from shipka.persistance.models.app import UserView, UserComponent

    views_path = os.path.join(app.config.get("BASEPATH"), 'apps', user_app, 'views.yaml')
    with open(views_path) as f:
        for document in yaml.safe_load_all(f):
            for view_def in document.get('views'):
                components = view_def.pop("components")
                if view_def.get('parent_user_view'):
                    parent_user_view_url_name = view_def.pop('parent_user_view')
                    parent_user_view = UserView.get_one_by(url_name=parent_user_view_url_name)
                    view_def['parent_user_view'] = parent_user_view
                uv = UserView.get_or_create(
                    user=admin_user,
                    **view_def,
                )
                for component_def in components:
                    UserComponent.get_or_create(
                        user=admin_user,
                        user_view=uv,
                        **component_def
                    )


def create_models(app, user_app):
    from shipka.persistance.models.models import META_MODEL_METHODS
    install_app = os.path.join(app.config.get("BASEPATH"),
                               'apps',
                               user_app)
    for meta_model in ["models", "instances"]:
        with open(os.path.join(install_app, "{}.yaml".format(meta_model))) as f:
            for data in yaml.safe_load_all(f):
                method = META_MODEL_METHODS[meta_model]
                method(data.get(meta_model, []))
                rel_method = META_MODEL_METHODS["{}_relationships".format(meta_model)]
                data_relationships = data.get("relationships")
                if data_relationships:
                    rel_method(data_relationships)


def shipka_populate(app, user_apps):
    from shipka.persistance import db
    from shipka.persistance.models.auth import Role, Group, Constants
    with app.app_context():
        sensitive_config = json.load(open(os.path.join(app.config.get("SECRETS_PATH"), 'app_sensitive.json')))
        admin_role = Role.get_or_create(name=Constants.ADMIN_ROLE)
        if hasattr(admin_role, 'is_new') and admin_role.is_new:
            admin_user = create_user(user_email=sensitive_config.get("admin_webapp_user"),
                                     password=sensitive_config.get('admin_webapp_password'),
                                     user_name="Admin",
                                     roles=[admin_role],
                                     app=app,
                                     user_apps=user_apps)
            normal_user = create_user(user_email=sensitive_config.get("webapp_user"),
                                      password=sensitive_config.get('admin_webapp_password'),
                                      user_name="User",
                                      app=app,
                                      user_apps=user_apps)

            public_group = Group(name="public")
            db.add(public_group)

            logged_in_group = Group(name="logged_in")
            admin_user.groups = [logged_in_group]
            normal_user.groups = [logged_in_group]
        db.push()
