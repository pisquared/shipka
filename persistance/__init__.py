import os

from flask_whooshee import Whooshee, AbstractWhoosheer
from time import sleep, time

from shipka.webapp.util import get_now_utc


class PersistentStorage(object):
    def __init__(self, app=None):
        self.sa_db = None
        if app:
            self.init_app(app)

    def init_app(self, app):
        from flask_sqlalchemy import SQLAlchemy
        self.sa_db = SQLAlchemy()
        # import pdb;pdb.set_trace()
        self.sa_db.init_app(app)
        if not os.path.isfile(app.config.get("DB_PATH")):
            os.makedirs(os.path.dirname(app.config.get("DB_PATH")), exist_ok=True)
            with app.app_context():
                # noinspection PyUnresolvedReferences
                from shipka.persistance.models.auth import User, Role
                # noinspection PyUnresolvedReferences
                from shipka.persistance.models.log import Log
                # noinspection PyUnresolvedReferences
                from shipka.persistance.models.app import UserView
                # noinspection PyUnresolvedReferences
                from shipka.persistance.models.models import ModelDefinition, \
                    ModelInstance, \
                    RelationshipDefinition, \
                    RelationshipInstance
                # noinspection PyUnresolvedReferences
                from shipka.persistance.models.tasks import Task

                self.sa_db.create_all()

    def add(self, instance):
        self.sa_db.session.add(instance)

    def delete(self, instance):
        now = get_now_utc()
        if hasattr(instance, 'deleted'):
            instance.deleted = True
            instance.deleted_ts = now
            self.sa_db.session.add(instance)
        else:
            self.sa_db.session.delete(instance)

    def push(self):
        self.sa_db.session.commit()

    def rollback(self):
        self.sa_db.session.rollback()


db = PersistentStorage()
whooshee = Whooshee()


def init_db(app):
    db.init_app(app)


def init_search(app, force_reindex=False):
    from shipka.persistance import whooshee
    from shipka.persistance.models.models import ModelInstance, model_instance_attr_dict

    whooshee.register_model(*[f for f in model_instance_attr_dict if f.startswith('field_')])(ModelInstance)

    whooshee.init_app(app)
    if force_reindex or not os.path.isdir(app.config.get("WHOOSHEE_DIR")):
        whooshee.reindex()


def search_reindex_cron(app):
    reindex_interval = app.config.get("SEARCH_REINDEX_INTERVAL_SECONDS", 10)
    while True:
        print("SEARCH: REINDEXING start.")
        start = time()
        whooshee.reindex()
        duration = time() - start
        print("SEARCH: REINDEXING end. Took {:1.2f} seconds".format(duration))
        print("SEARCH: REINDEXING sleeping {:1.2f} seconds".format(reindex_interval))

        sleep(reindex_interval)
