import datetime
import json
from json import JSONEncoder

from dateutil import tz
from flask_login import current_user
from sqlalchemy.ext.declarative import declared_attr, DeclarativeMeta
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy_utils import Choice

from shipka.persistance import db
from shipka.webapp.util import camel_case_to_snake_case, get_now_utc


class ModelJsonEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return str(o)
        return o.id


class AwareDateTime(db.sa_db.TypeDecorator):
    """
    Results returned as aware datetimes, not naive ones.
    """

    impl = db.sa_db.DateTime

    def process_result_value(self, value, dialect):
        if current_user and current_user.is_authenticated and value:
            tzinfo = tz.tzoffset(None, datetime.timedelta(seconds=current_user.tz_offset_seconds))
            return value.astimezone(tzinfo)
        return value


class Deletable(object):
    """
    This interface adds deletable properties to a model.
    """
    deleted = db.sa_db.Column(db.sa_db.Boolean, default=False)
    deleted_ts = db.sa_db.Column(AwareDateTime())

    @classmethod
    def get_filters(self, **kwargs):
        kwargs['deleted'] = kwargs.get('deleted', False)
        return kwargs

    def delete(self):
        self.deleted = True
        self.deleted_ts = get_now_utc()
        db.add(self)

    def undelete(self):
        self.deleted = False
        self.deleted_ts = None
        db.add(self)


class Datable(object):
    @classmethod
    def pre_create(cls, **kwargs):
        kwargs['created_ts'] = get_now_utc()
        return kwargs

    def pre_update(self, **kwargs):
        kwargs['updated_ts'] = get_now_utc()
        return kwargs

    created_ts = db.sa_db.Column(AwareDateTime())
    updated_ts = db.sa_db.Column(AwareDateTime())


class Searchable(object):
    @classmethod
    def search(cls, q, search_args=None, search_kwargs=None, *args, **kwargs):
        if not search_args:
            search_args = []
        if not search_kwargs:
            search_kwargs = {}
        return cls.filter_by(*args, **kwargs).whooshee_search(q, *search_args, **search_kwargs)


class ModelController(ModelJsonEncoder):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.sa_db.Column(db.sa_db.Integer, primary_key=True)

    _excluded_serialization = []

    @classmethod
    def pre_create(cls, **kwargs):
        return kwargs

    @classmethod
    def post_create(cls, instance, **kwargs):
        return instance

    def pre_update(self, **kwargs):
        return kwargs

    def post_update(self, **kwargs):
        return self

    @classmethod
    def create(cls, **kwargs):
        for klass in cls.__bases__:
            if hasattr(klass, "pre_create") and callable(klass.pre_create):
                kwargs = klass.pre_create(**kwargs)

        instance = cls(**kwargs)

        for klass in cls.__bases__:
            if hasattr(klass, "post_create") and callable(klass.post_create):
                instance = klass.post_create(instance, **kwargs)
        db.add(instance)
        return instance

    def update(self,  **kwargs):
        for klass in self.__class__.__bases__:
            if hasattr(klass, "pre_update") and callable(klass.pre_update):
                kwargs = klass.pre_update(self, **kwargs)

        for k, w in kwargs.items():
            setattr(self, k, w)

        for klass in self.__class__.__bases__:
            if hasattr(klass, "post_create") and callable(klass.post_create):
                klass.post_create(self, **kwargs)
        db.add(self)
        return self

    @classmethod
    def _filter_method(cls, filters=None, _with_lock=None, **kwargs):
        if filters is None:
            filters = []

        for klass in cls.__bases__:
            if hasattr(klass, 'get_filters') and callable(klass.get_filters):
                kwargs = klass.get_filters(**kwargs)

        order_by = kwargs.get("order_by", None)
        if order_by is not None:
            del kwargs['order_by']

        _query = cls.query
        if _with_lock:
            _query = _query.with_lockmode(_with_lock)

        rv_method = filterings = _query.filter_by(**kwargs).filter(*filters)

        if order_by is not None:
            rv_method = filterings.order_by(order_by)
        return rv_method

    @classmethod
    def filter_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs)

    @classmethod
    def get_by(cls, first=0, last=None, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()[first:last]

    @classmethod
    def get_one_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).first()

    @classmethod
    def get_one_with_lock_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, _with_lock="update", **kwargs).first()

    @classmethod
    def get_all_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()

    @classmethod
    def get_all(cls, **kwargs):
        return cls._filter_method(**kwargs).all()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.get_one_by(**kwargs)
        if not instance:
            instance = cls.create(**kwargs)
            instance.is_new = True
        return instance

    def serialize_flat(self, with_=None, depth=0, withs_used=None):
        """
        Serializes object to dict
        It will ignore fields that are not encodable (set them to 'None').

        It expands relations mentioned in with_ recursively up to MAX_DEPTH and tries to smartly ignore
        recursions by mentioning which with elements have already been used in previous depths
        :return:
        """

        MAX_DEPTH = 3

        def with_used_in_prev_depth(field, previous_depths):
            for previous_depth in previous_depths:
                if field in previous_depth:
                    return True
            return False

        def handle_withs(data, with_, depth, withs_used):
            if isinstance(data, InstrumentedList):
                if depth >= MAX_DEPTH:
                    return [e.serialize_flat() for e in data]
                else:
                    return [e.serialize_flat(with_=with_, depth=depth + 1, withs_used=withs_used) for e in data]
            else:
                if depth >= MAX_DEPTH:
                    return data.serialize_flat()
                else:
                    return data.serialize_flat(with_=with_, depth=depth + 1, withs_used=withs_used)

        if not with_:
            with_ = []
        if not withs_used:
            withs_used = []
        if isinstance(self.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            iterable_fields = [x for x in dir(self) if not x.startswith('_') and x not in ['metadata',
                                                                                           'item_separator',
                                                                                           'key_separator'] and x.islower()
                               and x not in self._excluded_serialization]
            for field in iterable_fields:
                data = self.__getattribute__(field)
                try:
                    if field in with_:
                        # this hanldes withs nested inside other models
                        if len(withs_used) < depth + 1:
                            withs_used.append([])
                        previous_depths = withs_used[:depth]
                        if with_used_in_prev_depth(field, previous_depths):
                            continue
                        withs_used[depth].append(field)
                        data = handle_withs(data, with_, depth, withs_used)
                    if isinstance(data, datetime.datetime):
                        data = str(data)
                    if isinstance(data, Choice):
                        data = data.code
                    json.dumps(data)  # this will fail on non-encodable values, like other classes
                    if isinstance(data, InstrumentedList):
                        continue  # pragma: no cover
                    fields[field] = data
                except TypeError:
                    pass  # Don't assign anything
            # a json-encodable dict
            return fields
