import json

from sqlalchemy_utils import ChoiceType

from shipka.persistance import db
from shipka.persistance.database import ModelController, ModelJsonEncoder
from shipka.persistance.models.auth import Ownable, get_user
from shipka.webapp.util import get_now_utc


class LogActions(object):
    MODEL_CREATED = "MODEL_CREATED"
    MODEL_UPDATED = "MODEL_UPDATED"
    MODEL_DELETED = "MODEL_DELETED"

    TASK_CREATED = "TASK_CREATED"
    TASK_STARTED = "TASK_STARTED"
    TASK_FINISHED = "TASK_FINISHED"


class LogLevels(object):
    INFO = "INFO"
    WARN = "WARN"
    ERROR = "ERROR"


class Log(db.sa_db.Model, ModelController, Ownable):
    """Logs all events happened on our end with somewhat structured data"""
    LOG_ACTIONS = [
        (LogActions.MODEL_CREATED, u'model_created'),
        (LogActions.MODEL_UPDATED, u'model_updated'),
        (LogActions.MODEL_DELETED, u'model_deleted'),
        (LogActions.TASK_CREATED, u'task_created'),
        (LogActions.TASK_STARTED, u'task_started'),
        (LogActions.TASK_FINISHED, u'task_finished'),
    ]

    LOG_LEVELS = [
        (LogLevels.INFO, u'info'),
        (LogLevels.WARN, u'warn'),
        (LogLevels.ERROR, u'error'),
    ]

    created_ts = db.sa_db.Column(db.sa_db.DateTime())  # UTC Datetime of this event

    action = db.sa_db.Column(ChoiceType(LOG_ACTIONS))
    level = db.sa_db.Column(ChoiceType(LOG_LEVELS), default=LogLevels.INFO)  # INFO|WARN|ERROR

    request_id = db.sa_db.Column(db.sa_db.Integer)  # if possible track as a part of request_id
    request_data = db.sa_db.Column(db.sa_db.JSON)  # serialized request data

    model = db.sa_db.Column(db.sa_db.String(255))  # the name of the model about which is this log entry
    model_data = db.sa_db.Column(db.sa_db.JSON)  # serialized model data

    task_data = db.sa_db.Column(db.sa_db.JSON)  # serialized task data


class Loggable(object):
    @classmethod
    def post_create(cls, instance, **kwargs):
        model = instance.__class__.__name__
        if model in ['Log']:
            return instance
        log = Log.create(created_ts=get_now_utc(),
                         action=LogActions.MODEL_CREATED,
                         model=model,
                         model_data=json.dumps(kwargs, cls=ModelJsonEncoder))
        if issubclass(cls, Ownable):
            user = get_user()
            log.user = user
        db.add(log)
        return instance
