from flask import session, current_app
from flask_login import current_user
from flask_security import RoleMixin, UserMixin
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy_utils import ChoiceType

from shipka.persistance import db
from shipka.persistance.database import ModelController, AwareDateTime, Datable, Deletable


class Constants(object):
    ADMIN_ROLE = 'admin'


class Proxy(object):
    def __init__(self):
        self.value = None

    def __call__(self, value):
        self.value = value
        return self

    def set(self, value):
        self.value = value

    def get(self):
        return self.value

    def __enter__(self):
        self.set(self.value)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.set(None)


roles_users = db.sa_db.Table('roles_users',
                             db.sa_db.Column('user_id', db.sa_db.Integer(), db.sa_db.ForeignKey('user.id')),
                             db.sa_db.Column('role_id', db.sa_db.Integer(), db.sa_db.ForeignKey('role.id')))

roles_permissions = db.sa_db.Table('roles_permissions',
                                   db.sa_db.Column('role_id', db.sa_db.Integer(), db.sa_db.ForeignKey('role.id')),
                                   db.sa_db.Column('permission_id', db.sa_db.Integer(),
                                                   db.sa_db.ForeignKey('permission.id')))

groups_users = db.sa_db.Table('groups_users',
                              db.sa_db.Column('user_id', db.sa_db.Integer(), db.sa_db.ForeignKey('user.id')),
                              db.sa_db.Column('group_id', db.sa_db.Integer(), db.sa_db.ForeignKey('group.id')))

global_user = Proxy()


class Role(db.sa_db.Model, ModelController, RoleMixin):
    name = db.sa_db.Column(db.sa_db.Unicode, unique=True)
    description = db.sa_db.Column(db.sa_db.Unicode)

    permissions = db.sa_db.relationship('Permission', secondary=roles_permissions,
                                        backref=db.sa_db.backref('roles', lazy='dynamic'))


class User(db.sa_db.Model, ModelController, UserMixin):
    email = db.sa_db.Column(db.sa_db.Unicode, unique=True)
    password = db.sa_db.Column(db.sa_db.Unicode)

    active = db.sa_db.Column(db.sa_db.Boolean(), default=True)
    confirmed_at = db.sa_db.Column(db.sa_db.DateTime())

    timezone = db.sa_db.Column(db.sa_db.String, default='UTC')
    tz_offset_seconds = db.sa_db.Column(db.sa_db.Integer, default=0)
    locale = db.sa_db.Column(db.sa_db.String(4), default='en')

    name = db.sa_db.Column(db.sa_db.Unicode)
    profile_image_url = db.sa_db.Column(db.sa_db.String)

    roles = db.sa_db.relationship('Role', secondary=roles_users,
                                  backref=db.sa_db.backref('users', lazy='dynamic'))

    groups = db.sa_db.relationship('Group', secondary=groups_users,
                                   backref=db.sa_db.backref('users', lazy='dynamic'))


class Group(db.sa_db.Model, ModelController):
    name = db.sa_db.Column(db.sa_db.Unicode, unique=True)
    description = db.sa_db.Column(db.sa_db.Unicode)


class Ownable(object):
    @classmethod
    def get_filters(cls, **kwargs):
        user = get_user(create=False)
        if user:
            kwargs['user_id'] = user.id
        return kwargs

    @classmethod
    def pre_create(cls, **kwargs):
        user = get_user(create=False)
        kwargs['user'] = user
        return kwargs

    @declared_attr
    def user_id(self):
        return db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('user.id'))

    @declared_attr
    def user(self):
        return db.sa_db.relationship("User")


class Resource(db.sa_db.Model, ModelController):
    IDENTIFIER_TYPES = [
        ("ROUTE", u'route'),
    ]

    name = db.sa_db.Column(db.sa_db.Unicode, unique=True)

    identifier_type = db.sa_db.Column(ChoiceType(IDENTIFIER_TYPES))
    identifier = db.sa_db.Column(db.sa_db.Unicode)

    def __repr__(self):
        return '<Resource %r>' % self.name


class Permission(db.sa_db.Model, ModelController, Ownable):
    # You don't assign permissions to users directly.
    # Instead, you assign them a Role which contains one or more permissions.

    VERBS = [
        ("GET", u'get'),
        ("LIST", u'list'),
        ("CREATE", u'create'),
        ("UPDATE", u'update'),
        ("DELETE", u'delete'),
    ]

    resource_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('resource.id'))
    resource = db.sa_db.relationship("Resource", backref="permissions")

    verb = db.sa_db.Column(ChoiceType(VERBS))

    def __repr__(self):
        return '<Permission %r>' % self.name


class Quota(db.sa_db.Model, ModelController, Ownable):
    LIMIT_UNITS = [
        ("MB", u'megabytes'),
        ("COUNT", u'delete'),
    ]

    LIMIT_PERIOD = [
        ("DAY", u'day'),
        ("HOUR", u'hour'),
        ("MINUTE", u'minute'),
        ("SECOND", u'second'),
    ]

    resource_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('resource.id'))
    resource = db.sa_db.relationship("Resource", backref="quotas")

    limit_unit = db.sa_db.Column(ChoiceType(LIMIT_UNITS))
    limit_count = db.sa_db.Column(db.sa_db.Integer)
    limit_period = db.sa_db.Column(ChoiceType(LIMIT_PERIOD))

    def __repr__(self):
        return '<Quota %r>' % self.name


class Notification(db.sa_db.Model, ModelController, Datable, Deletable, Ownable):
    message_template = db.sa_db.Column(db.sa_db.Unicode)
    message_context = db.sa_db.Column(db.sa_db.Unicode)

    url = db.sa_db.Column(db.sa_db.Unicode)

    schedule_ts = db.sa_db.Column(AwareDateTime())

    is_fired = db.sa_db.Column(db.sa_db.Boolean, default=False)
    fired_ts = db.sa_db.Column(AwareDateTime())

    is_read = db.sa_db.Column(db.sa_db.Boolean, default=False)
    read_ts = db.sa_db.Column(AwareDateTime())

    def __repr__(self):
        return '<Notification %r - %r>' % (self.id, self.message_template)


class Contextable(object):
    @declared_attr
    def context_id(self):
        return db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('context.id'))

    @declared_attr
    def context(self):
        return db.sa_db.relationship("Context")


class Context(db.sa_db.Model, ModelController, Ownable, Datable):
    name = db.sa_db.Column(db.sa_db.Unicode)

    filters = db.sa_db.Column(db.sa_db.Unicode)

    def __repr__(self):
        return '<Context %r>' % self.id


class UserSettings(db.sa_db.Model, ModelController, Ownable, Datable):
    last_context_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('context.id'))
    last_context = db.sa_db.relationship("Context")


def get_user(create=None):
    """
    Gets the currently saved user - either anonymous or authenticated.
    If no user is authenticated, create one with the next id and store
    it in the session.
    :return:
    """
    if create is None:
        create = current_app.config.get("CREATE_ANONYMOUS_USER")
    if global_user.get():
        return global_user.get()
    if current_user and current_user.is_authenticated:
        return current_user
    user_id = session.get('anonymous_user_id')
    if user_id:
        user = User.get_one_by(id=int(user_id))
        if user:
            return user
    # naively check if bot
    # user_agent = ua_parse(request.user_agent.string)
    # if user_agent.is_bot:
    #     return None
    if not create:
        return None
    user = User.create()
    db.add(user)
    db.push()
    session['anonymous_user_id'] = user.id
    return user
