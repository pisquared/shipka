from shipka.persistance import db
from shipka.persistance.database import ModelController
from shipka.persistance.models.auth import Ownable, Contextable


class App(db.sa_db.Model, ModelController):
    name = db.sa_db.Column(db.sa_db.String)


class UserApp(db.sa_db.Model, ModelController, Ownable):
    app_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('app.id'))
    app = db.sa_db.relationship("App", backref='user_apps')


class UserView(db.sa_db.Model, ModelController, Ownable, Contextable):
    name = db.sa_db.Column(db.sa_db.String)
    url_name = db.sa_db.Column(db.sa_db.String)
    icon = db.sa_db.Column(db.sa_db.String)

    on_main = db.sa_db.Column(db.sa_db.Boolean, default=False)  # should it be on the main menu
    order = db.sa_db.Column(db.sa_db.Integer)

    parent_user_view_id = db.sa_db.Column(db.sa_db.Integer,
                                          db.sa_db.ForeignKey('user_view.id'))
    parent_user_view = db.sa_db.relationship("UserView", remote_side='UserView.id', backref='subviews')

    def __repr__(self):
        return '<UserView %r - %r>' % (self.id, self.name)


class UserComponent(db.sa_db.Model, ModelController, Ownable):
    name = db.sa_db.Column(db.sa_db.Unicode)
    cols = db.sa_db.Column(db.sa_db.Integer)

    user_view_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('user_view.id'))
    user_view = db.sa_db.relationship("UserView", backref='components')

    def __repr__(self):
        return '<UserComponent %r - %r>' % (self.id, self.name)
