from shipka.persistance import db
from shipka.persistance.database import ModelController, Datable
from shipka.persistance.models.auth import Ownable


class Task(db.sa_db.Model, ModelController, Datable, Ownable):
    uuid = db.sa_db.Column(db.sa_db.Unicode)

    name = db.sa_db.Column(db.sa_db.Unicode)
    kwargs = db.sa_db.Column(db.sa_db.Unicode)

    status = db.sa_db.Column(db.sa_db.Unicode)
    percent_done = db.sa_db.Column(db.sa_db.Unicode)
    progress = db.sa_db.Column(db.sa_db.Unicode)
    result = db.sa_db.Column(db.sa_db.Unicode)
