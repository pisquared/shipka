import json
import operator
import uuid
from collections import defaultdict

from sqlalchemy_utils import ChoiceType

from shipka.persistance.database import Datable, db, Searchable
from shipka.persistance.database import ModelController
from shipka.persistance.models.auth import Ownable
from shipka.persistance.models.log import Loggable
from shipka.webapp.util import safe_list, transform_time, transform_time_interval

FIELD_COUNT = 32


class FieldType(object):
    TYPE_SHORT_TEXT = u"SHORT_TEXT"
    TYPE_LONG_TEXT = u"LONG_TEXT"
    TYPE_INTEGER = u"INTEGER"
    TYPE_TIMER = u"TIMER"
    TYPE_COLOR = u"COLOR"
    TYPE_CHECKBOX = u"CHECKBOX"

    TYPE_FILE = u"FILE"

    # iso-8601: 2007-04-05T12:30-02:00
    TYPE_DATETIME = u"DATETIME"

    # iso-8601: -02:00
    TYPE_TIMEZONE = u"TIMEZONE"

    # Always represent as seconds in backend
    # iso8601 duration is ambiguous for the duration of year/month even day
    TYPE_TIME_DURATION = u"TIME_DURATION"

    # TIME_INTERVAL contains two time-like values - start and end
    # where end >= start.
    #
    # interval_type: absolute (default)
    #   start = iso-8601 datetime
    #   end = iso-8601 datetime
    #
    # interval_type: relative:
    #   start = 50 seconds
    #   end = 350 seconds
    #
    TYPE_TIME_INTERVAL = u"TIME_INTERVAL"

    # TIME_REPEATING_INTERVAL contains:
    #   start_interval: iso-8601
    #   repeating_pattern: [calendar|cron|iso-8601]
    #   end_interval: iso-8601
    #
    # repeating_type: calendar
    #   follow a google-calendar-like repeating interval
    #
    # repeating_type: cron
    #   follow a cron-like description of repeating times
    #   [minute] [hour] [day of month] [month] [day of week]
    #
    # repeating_type: iso-8601
    #   For example, to repeat the interval of
    #   "P1Y2M10DT2H30M" five times starting at "2008-03-01T13:00:00Z",
    #   use "R5/2008-03-01T13:00:00Z/P1Y2M10DT2H30M"
    TYPE_TIME_REPEATING_INTERVAL = u"TIME_REPEATING_INTERVAL"

    TYPE_CHOICE_SET = u"CHOICE_SET"
    TYPE_RELATIONSHIP = u"RELATIONSHIP"


MODEL_TYPE_OPTIONS = [
    (FieldType.TYPE_SHORT_TEXT, u'short_text'),
    (FieldType.TYPE_LONG_TEXT, u'long_text'),
    (FieldType.TYPE_INTEGER, u'integer'),
    (FieldType.TYPE_TIMER, u'timer'),
    (FieldType.TYPE_COLOR, u'color'),
    (FieldType.TYPE_FILE, u'file'),
    (FieldType.TYPE_CHECKBOX, u'checkbox'),
    (FieldType.TYPE_DATETIME, u'datetime'),
    (FieldType.TYPE_TIMEZONE, u'timezone'),
    (FieldType.TYPE_TIME_INTERVAL, u'time_interval'),
    (FieldType.TYPE_TIME_DURATION, u'time_duration'),
    (FieldType.TYPE_TIME_REPEATING_INTERVAL, u'time_repeating_interval'),
    (FieldType.TYPE_CHOICE_SET, u'choice_set'),
    (FieldType.TYPE_RELATIONSHIP, u'relationship'),
]


def serialize_model_definition(self):
    fields = {}
    for num in range(1, FIELD_COUNT + 1):
        field = dict()
        label = getattr(self, "field_{:02d}_label".format(num))
        if label is not None:
            field["num"] = "{:02d}".format(num)
            field["label"] = label
            field_type = getattr(self, "field_{:02d}_type".format(num))
            field["type"] = field_type
            options = getattr(self, "field_{:02d}_options".format(num))
            if options:
                options = json.loads(options)
            field["options"] = options
            if field_type == FieldType.TYPE_CHOICE_SET:
                field['choices'] = inject_choices(options, None)
            fields[label] = field
    return {
        "name": self.name,
        "inherits": self.inherits,
        "fields": fields,
    }


model_definition_attr_dict = dict(
    namespace=db.sa_db.Column(db.sa_db.Unicode),
    name=db.sa_db.Column(db.sa_db.Unicode),
    inherits=db.sa_db.Column(db.sa_db.Unicode),

    latest_created=db.sa_db.Column(db.sa_db.Unicode),
    latest_updated=db.sa_db.Column(db.sa_db.Unicode),
    latest_deleted=db.sa_db.Column(db.sa_db.Unicode),

    serialize=serialize_model_definition,

    search_snippets_from=db.sa_db.Column(db.sa_db.Unicode),
    search_link_from=db.sa_db.Column(db.sa_db.Unicode),
    search_view_url_name=db.sa_db.Column(db.sa_db.Unicode),
    search_view_params=db.sa_db.Column(db.sa_db.Unicode),
)

model_instance_attr_dict = dict(
    uuid=db.sa_db.Column(db.sa_db.Unicode),
    model_definition_id=db.sa_db.Column(db.sa_db.Integer,
                                        db.sa_db.ForeignKey('model_definition.id')),
    model_definition=db.sa_db.relationship("ModelDefinition",
                                           backref="model_instances"),
)

for name in range(1, FIELD_COUNT + 1):
    model_definition_attr_dict["field_{:02d}_label".format(name)] = db.sa_db.Column(db.sa_db.Unicode)
    model_definition_attr_dict["field_{:02d}_type".format(name)] = db.sa_db.Column(ChoiceType(MODEL_TYPE_OPTIONS))
    model_definition_attr_dict["field_{:02d}_options".format(name)] = db.sa_db.Column(db.sa_db.Unicode)
    model_instance_attr_dict["field_{:02d}_value".format(name)] = db.sa_db.Column(db.sa_db.Unicode)

ModelDefinition = type("ModelDefinition",
                       (db.sa_db.Model, ModelController, Datable, Loggable),
                       model_definition_attr_dict,
                       )

ModelInstance = type("ModelInstance",
                     (db.sa_db.Model, ModelController, Datable, Searchable, Loggable),
                     model_instance_attr_dict
                     )


class ModelOperation(db.sa_db.Model, ModelController, Datable, Ownable):
    OPERATION_CREATED = "CREATED"
    OPERATION_UPDATED = "UPDATED"
    OPERATION_DELETED = "DELETED"

    TYPE_OPERATIONS = [
        (OPERATION_CREATED, u'created'),
        (OPERATION_UPDATED, u'updated'),
        (OPERATION_DELETED, u'deleted'),
    ]

    STATUS_PENDING = "PENDING"
    STATUS_SUCCESS = "SUCCESS"
    STATUS_FAILED = "FAILED"

    TYPE_STATUS = [
        (STATUS_PENDING, u'pending'),
        (STATUS_SUCCESS, u'success'),
        (STATUS_FAILED, u'failed'),
    ]

    model_definition_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('model_definition.id'))
    model_definition = db.sa_db.relationship("ModelDefinition", backref="model_operations")
    operation = db.sa_db.Column(ChoiceType(TYPE_OPERATIONS))
    status = db.sa_db.Column(ChoiceType(TYPE_STATUS), default=STATUS_PENDING)
    uuid = db.sa_db.Column(db.sa_db.Unicode)


class RelationshipDefinition(db.sa_db.Model, ModelController, Ownable, Datable):
    TYPE_OPTIONS = [
        ("ONE_TO_MANY", u'many_to_one'),
        ("MANY_TO_ONE", u'many_to_one'),
        ("MANY_TO_MANY", u'many_to_many'),
    ]

    from_model_definition_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('model_definition.id'))
    from_model_definition = db.sa_db.relationship("ModelDefinition", backref="relationships_from",
                                                  foreign_keys=[from_model_definition_id])

    to_model_definition_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('model_definition.id'))
    to_model_definition = db.sa_db.relationship("ModelDefinition", backref="relationships_to",
                                                foreign_keys=[to_model_definition_id])

    name = db.sa_db.Column(db.sa_db.Unicode)
    backref_from = db.sa_db.Column(db.sa_db.Unicode)
    backref_to = db.sa_db.Column(db.sa_db.Unicode)

    relationship_type = db.sa_db.Column(ChoiceType(TYPE_OPTIONS))


class RelationshipInstance(db.sa_db.Model, ModelController, Ownable, Datable):
    relationship_definition_id = db.sa_db.Column(db.sa_db.Integer,
                                                 db.sa_db.ForeignKey('relationship_definition.id'))
    relationship_definition = db.sa_db.relationship("RelationshipDefinition",
                                                    backref="relationship_definitions")

    from_model_instance_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('model_instance.id'))
    from_model_instance = db.sa_db.relationship("ModelInstance", backref="relationships_from",
                                                foreign_keys=[from_model_instance_id])

    to_model_instance_id = db.sa_db.Column(db.sa_db.Integer, db.sa_db.ForeignKey('model_instance.id'))
    to_model_instance = db.sa_db.relationship("ModelInstance", backref="relationships_to",
                                              foreign_keys=[to_model_instance_id])


# ============= API
# ==============================================================================


class Filter(object):
    EQUALS = operator.eq
    LESS_THAN = operator.lt
    GREATER_THAN = operator.gt

    def __init__(self, label=None, comparison=None, value=None):
        self.label = label
        self.comparison = comparison or Filter.EQUALS
        self.value = value


class Field(object):
    def __init__(self, label=None, type=None, options=None, value=None):
        self.label = label
        self.type = type
        self.options = options or "{}"
        self.value = value


class DatabaseException(Exception):
    pass


class Model(object):
    MODEL_ACTIONS = defaultdict(dict)
    MODEL_PRE_CREATE_HOOKS = defaultdict(list)
    MODEL_POST_CREATE_HOOKS = defaultdict(list)

    @staticmethod
    def create_model(model_name, fields, model_inherits=None, namespace="default", **kwargs):
        for num, field in enumerate(fields):
            num += 1
            assert isinstance(field, Field)
            kwargs["field_{:02d}_label".format(num)] = field.label
            kwargs["field_{:02d}_type".format(num)] = field.type
            kwargs["field_{:02d}_options".format(num)] = field.options

        model_definition = ModelDefinition.create(name=model_name,
                                                  namespace=namespace,
                                                  inherits=model_inherits,
                                                  **kwargs)
        return model_definition.serialize()

    @classmethod
    def create_from_dict(cls, definitions):
        models_serialized = []
        for model_def in definitions:
            model_name = model_def.get("name")
            model_shared = model_def.get("shared")
            # TODO
            # print(model_name)
            # print(model_shared)
            # print(cls.query(model_name))
            if model_shared and cls.query(model_name):
                continue
            fields_def = model_def.get("fields", [])
            fields = []
            for field_def in fields_def:
                field_label = field_def.get("label")
                field_type = field_def.get("type")
                field_options = json.dumps(field_def.get("options", {}))
                fields.append(
                    Field(label=field_label, type=field_type, options=field_options),
                )
            kwargs = dict()
            search = model_def.get("search")
            if search:
                kwargs['search_link_from'] = search.get("link_from")
                kwargs['search_snippets_from'] = json.dumps(search.get("snippets_from"))
                view = search.get("view")
                if view:
                    kwargs['search_view_url_name'] = view.get("url_name")
                    kwargs['search_view_params'] = json.dumps(view.get("params", {}))
            model = cls.create_model(model_name=model_name, fields=fields, **kwargs)
            models_serialized.append(model)
            db.push()
        return models_serialized

    @staticmethod
    def _create_model_operation(model_definition, operation, uuid):
        model_operation = ModelOperation.create(
            model_definition=model_definition,
            operation=operation,
            uuid=uuid)
        db.push()
        return model_operation

    @staticmethod
    def _update_model_operation(model_operation, status):
        model_operation.update(status=status)
        db.push()
        return model_operation

    @classmethod
    def create_instance(cls, model_name, fields, namespace="default"):
        model_definition = ModelDefinition.get_one_by(name=model_name, namespace=namespace)
        model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                        for num in range(1, FIELD_COUNT + 1)}
        kwargs = dict()
        for field in fields:
            assert isinstance(field, Field)
            if field.label is not None:
                field_num = model_labels.get(field.label)
                if field_num:
                    kwargs["field_{:02d}_value".format(field_num)] = field.value

        creation_uuid = str(uuid.uuid4())

        model_operation = cls._create_model_operation(model_definition,
                                                      ModelOperation.OPERATION_CREATED,
                                                      creation_uuid)
        model_instance = ModelInstance.create(
            uuid=creation_uuid,
            model_definition=model_definition,
            **kwargs)
        last_created_uuid = model_definition.latest_created
        try:
            model_definition.latest_created = creation_uuid
            db.add(model_definition)
            db.push()
            cls._update_model_operation(model_operation, ModelOperation.STATUS_SUCCESS)
        except Exception as e:
            print(e)
            db.rollback()
            model_definition.latest_created = last_created_uuid
            db.add(model_definition)
            cls._update_model_operation(model_operation, ModelOperation.STATUS_FAILED)
        return model_instance

    @classmethod
    def _update_instance(cls, model_definition, instance, updates):
        model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                        for num in range(1, FIELD_COUNT + 1)}
        final_updates = {}
        for field in updates:
            field_num = model_labels.get(field.label)
            if field_num:
                final_updates["field_{:02d}_value".format(field_num)] = field.value
        update_uuid = instance.uuid
        model_operation = cls._create_model_operation(model_definition,
                                                      ModelOperation.OPERATION_UPDATED,
                                                      update_uuid)
        instance.update(**final_updates)
        db.add(instance)
        last_updated_uuid = model_definition.latest_updated
        try:
            model_definition.latest_updated = update_uuid
            db.add(model_definition)
            db.push()
            cls._update_model_operation(model_operation, ModelOperation.STATUS_SUCCESS)
        except Exception as e:
            print(e)
            db.rollback()
            model_definition.latest_updated = last_updated_uuid
            db.add(model_definition)
            cls._update_model_operation(model_operation, ModelOperation.STATUS_FAILED)

    @classmethod
    def delete_instance(cls, model_name, instance_uuid):
        # TODO
        model_definition = ModelDefinition.get_one_by(name=model_name)
        model_instance = ModelInstance.get_one_by(model_definition=model_definition,
                                                  uuid=instance_uuid)
        model_instance.delete()
        delete_uuid = instance_uuid
        model_operation = cls._create_model_operation(model_definition,
                                                      ModelOperation.OPERATION_DELETED,
                                                      delete_uuid)
        last_deleted_uuid = model_definition.latest_deleted
        try:
            model_definition.latest_deleted = delete_uuid
            db.add(model_definition)
            db.push()
            cls._update_model_operation(model_operation, ModelOperation.STATUS_SUCCESS)
        except Exception as e:
            print(e)
            db.rollback()
            model_definition.latest_deleted = last_deleted_uuid
            db.add(model_definition)
            cls._update_model_operation(model_operation, ModelOperation.STATUS_FAILED)

    @classmethod
    def create_instance_from_dict(cls, definitions):
        instances_serialized = []
        for instance_def in definitions:
            model_name = instance_def.get("model")
            fields_def = instance_def.get("fields", [])
            fields = cls.dict_to_fields(model_name, fields_def)
            instance = cls.create_instance(model_name=model_name, fields=fields)
            instances_serialized.append(instance)
        return instances_serialized

    @classmethod
    def update(cls, model_name, updates, instance_uuid=None, filters=None, models=None):
        if not models:
            models = cls.query(model_name, instance_uuid, filters).instances

        if type(updates) is dict:
            updates = Model.dict_to_fields(model_name, updates)

        for model in models:
            model_definition = ModelDefinition.get_one_by(name=model_name)
            cls._update_instance(model_definition, model._instance, updates)
        db.push()



    @classmethod
    def _filter(cls, model_definition, instance_uuid=None, filters=None):
        filters = filters or []
        model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                        for num in range(1, FIELD_COUNT + 1)}
        if instance_uuid:
            instances = ModelInstance.get_all_by(model_definition=model_definition,
                                                 uuid=instance_uuid)
        else:
            resolved_filters = {}
            for filter in filters:
                num = model_labels.get(filter.label)
                resolved_filters["field_{:02d}_value".format(num)] = filter.value
            instances = ModelInstance.get_all_by(model_definition=model_definition,
                                                 **resolved_filters)
        return instances

    @classmethod
    def _transform_value(cls, field_type, field_options, value):
        try:
            if field_type == FieldType.TYPE_TIME_INTERVAL:
                return transform_time_interval(value)
            elif field_type == FieldType.TYPE_TIME_DURATION:
                return transform_time(value)
            elif field_type == FieldType.TYPE_CHOICE_SET:
                return inject_choices(field_options, value)
        except Exception as e:
            print(e)
        return {'value': value}

    @classmethod
    def _serialize_instance(cls, model_definition, instance, rel_backrefs=None):
        instance_fields = dict(_id=instance.id, uuid=instance.uuid)
        instance_fields = {k: v for k, v in instance_fields.items()
                           if not k.startswith('field_') and not k.endswith('_value')}
        mds = model_definition.serialize()
        for num in range(1, FIELD_COUNT + 1):
            label = getattr(model_definition, "field_{:02d}_label".format(num))
            if label is not None:
                value = getattr(instance, "field_{:02d}_value".format(num))
                field_type = getattr(model_definition, "field_{:02d}_type".format(num))
                field_options = getattr(model_definition, "field_{:02d}_options".format(num))
                try:
                    field_options = json.loads(field_options)
                except:
                    field_options = {}
                instance_fields[label] = cls._transform_value(field_type, field_options, value)
                instance_fields[label]['field_def'] = mds['fields'][label]
        if rel_backrefs:
            for direction, backrefs in rel_backrefs.items():
                for backref in backrefs:
                    instance_fields[backref.label] = backref.get(direction, instance.uuid)
        instance_fields["_instance"] = instance
        # merging dicts
        klass_attributes = {**instance_fields, **{
            "__dict": instance_fields,
        }}
        return type(model_definition.name, (), klass_attributes)

    @classmethod
    def _serialize_instances(cls, model_definition, instances):
        rel_backrefs = cls._get_rel_backrefs(model_definition)
        instances_serialized = list()
        for instance in instances:
            instances_serialized.append(cls._serialize_instance(model_definition, instance, rel_backrefs))
        return instances_serialized

    @classmethod
    def _get_rel_backrefs(cls, model_definition):
        rel_backrefs = {"from": [], "to": []}
        for direction in rel_backrefs.keys():
            relationships = getattr(model_definition, "relationships_{}".format(direction))
            for rel_def in relationships:
                backref_name = getattr(rel_def, "backref_{}".format(direction))
                rel_backrefs[direction].append(type(
                    backref_name,
                    (), {
                        "label": backref_name,
                        "get": lambda direction, instance_id: lambda: (
                            cls.get_relationships(rel_def.name, direction, instance_id)),
                    }))
        return rel_backrefs

    @classmethod
    def serialize_model_def(cls, model_name):
        model_definition = ModelDefinition.get_one_by(name=model_name)
        assert model_definition is not None

        return model_definition.serialize()

    @classmethod
    def get_all(cls):
        model_definitions = ModelDefinition.get_all_by()
        return [md.serialize() for md in model_definitions]

    @classmethod
    def query(cls, model_name, instance_uuid=None, filters=None):
        model_definition = ModelDefinition.get_one_by(name=model_name)
        if not model_definition:
            return None

        instances = cls._filter(model_definition, instance_uuid, filters)

        instances_serialized = cls._serialize_instances(model_definition, instances)
        return type(model_name, (),
                    {
                        "model": model_definition.serialize(),
                        "instances": safe_list(instances_serialized),
                    })

    @classmethod
    def search(cls, model_name, q):
        model_definition = ModelDefinition.get_one_by(name=model_name)
        if not model_definition:
            return {
                "instances": [],
            }

        instances = ModelInstance.search(q, model_definition=model_definition).all()

        instances_serialized = cls._serialize_instances(model_definition, instances)
        return type(model_name, (),
                    {
                        "model": model_definition.serialize(),
                        "instances": safe_list(instances_serialized),
                        "search": {
                            "link_from": model_definition.search_link_from,
                            "snippets_from": json.loads(model_definition.search_snippets_from),
                            "url_name": model_definition.search_view_url_name,
                            "params": json.loads(model_definition.search_view_params),
                        }
                    })

    @classmethod
    def dict_to_fields(cls, model_name, payload):
        model_definition = ModelDefinition.get_one_by(name=model_name)
        assert model_definition is not None

        model_def_ser = model_definition.serialize()
        rv = []
        payload_iter = set([k.split('__')[0] for k in payload.keys()])
        for field_name in payload_iter:
            field_def = model_def_ser['fields'].get(field_name)
            field_type = field_def.get("type")
            if field_type == FieldType.TYPE_TIME_INTERVAL:
                duration_start = payload.get("{}__start".format(field_name))
                duration_end = payload.get("{}__end".format(field_name))
                rv.append(Field(label=field_name, value="{}/{}".format(duration_start, duration_end)))
            else:
                value = str(payload.get(field_name))
                rv.append(Field(label=field_name, value=value))
        return rv

    @classmethod
    def create_model_relationships_from_dict(cls, definitions):
        for rel_def in definitions:
            rel_name = rel_def.get("name")
            from_model_name = rel_def.get("from_model")
            from_model = ModelDefinition.get_one_by(name=from_model_name)
            to_model_name = rel_def.get("to_model")
            to_model = ModelDefinition.get_one_by(name=to_model_name)
            rel_type = rel_def.get("type")
            RelationshipDefinition.create(
                name=rel_name,
                from_model_definition=from_model,
                to_model_definition=to_model,
                relationship_type=rel_type,
                backref_from=rel_def.get("backref_from"),
                backref_to=rel_def.get("backref_to"),
            )
        db.push()

    @classmethod
    def create_instance_relationships_from_dict(cls, definitions):
        if not definitions:
            return
        for rel_inst_def in definitions:
            rel_name = rel_inst_def.get("name")
            rel_def = RelationshipDefinition.get_one_by(name=rel_name)
            from_filters = []
            for field, value in rel_inst_def.get("from_filters").items():
                from_filters.append(Filter(label=field, value=value))
            from_model = cls.query(rel_def.from_model_definition.name,
                                   filters=from_filters)
            from_model_instance_id = from_model.instances[0].uuid
            to_filters = []
            for field, value in rel_inst_def.get("to_filters").items():
                to_filters.append(Filter(label=field, value=value))
            to_model = cls.query(rel_def.to_model_definition.name,
                                 filters=to_filters)
            to_model_instance_id = to_model.instances[0].uuid
            RelationshipInstance.create(
                relationship_definition=rel_def,
                from_model_instance_id=from_model_instance_id,
                to_model_instance_id=to_model_instance_id,
            )
        db.push()

    @classmethod
    def create_relationship(cls, relationship_name, from_model, to_model):
        rel_def = RelationshipDefinition.get_one_by(name=relationship_name)
        RelationshipInstance.create(
            relationship_definition=rel_def,
            from_model_instance_id=from_model.uuid,
            to_model_instance_id=to_model.uuid,
        )
        db.push()


    @classmethod
    def get_relationships(cls, rel_name, direction, from_instance_id):
        o_direction = "to" if direction == "from" else "from"
        rel_def = RelationshipDefinition.get_one_by(name=rel_name)
        o_model_definition = getattr(rel_def, "{}_model_definition".format(o_direction))
        o_model_name = o_model_definition.name
        rel_inst_filters = {
            "relationship_definition": rel_def,
            "{}_model_instance_id".format(direction): from_instance_id,
        }
        rel_instances = RelationshipInstance.get_all_by(**rel_inst_filters)
        instances = []
        for rel_instance in rel_instances:
            o_uuid = getattr(rel_instance, "{}_model_instance_id".format(o_direction))
            instances += ModelInstance.get_all_by(model_definition=o_model_definition,
                                                  uuid=o_uuid)
        instances_serialized = cls._serialize_instances(o_model_definition, instances)

        return type(o_model_name, (), {
            "model": o_model_definition.serialize(),
            "instances": instances_serialized,
        })

    @classmethod
    def register_action(cls, model_name, action_name, method):
        cls.MODEL_ACTIONS[model_name][action_name] = method

    @classmethod
    def do_action(cls, model_name, action_name, instance_uuid=None, fields=None, filters=None):
        action_method = cls.MODEL_ACTIONS.get(model_name, {}).get(action_name, None)
        assert action_method is not None

        if instance_uuid:
            result_model = cls.query(model_name, instance_uuid)
        elif filters:
            result_model = cls.query(model_name, filters=filters)
        else:
            result_model = cls.query(model_name)

        action_method(result_model, fields=fields)

    @classmethod
    def register_pre_create_hook(cls, model_name, hook):
        return cls.MODEL_PRE_CREATE_HOOKS[model_name].append(hook)

    @classmethod
    def get_pre_create_hooks(cls, model_name):
        return cls.MODEL_PRE_CREATE_HOOKS[model_name]

    @classmethod
    def register_post_create_hook(cls, model_name, hook):
        return cls.MODEL_POST_CREATE_HOOKS[model_name].append(hook)

    @classmethod
    def get_post_create_hooks(cls, model_name):
        return cls.MODEL_POST_CREATE_HOOKS[model_name]


META_MODEL_METHODS = {
    "models": Model.create_from_dict,
    "models_relationships": Model.create_model_relationships_from_dict,
    "instances": Model.create_instance_from_dict,
    "instances_relationships": Model.create_instance_relationships_from_dict,
}


def inject_choices(options, value, model_definition=None):
    choices = []
    if 'from_existing' in options and options['from_existing']:
        choices += []
        # TODO: HIGHLY UNOPTIMAL - CACHE!!!!!!!!!
        # if model_definition:
        #     instances = Model.query(model_name=model_definition.name).get('instances')
        #     choices += [getattr(i, field_name) for i in instances]
    if 'initial_choices' in options and type(options.get('initial_choices')) is dict:
        choices += [(k, v) for k, v in options['initial_choices'].items()]
    if value:
        return {
            'selection': {
                # TODO: value should come from a key-value pair
                'key': value,
                'value': value,
            },
            'choices': set(choices),
        }
    return set(choices)
