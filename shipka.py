import json
import os
import sys

config_pip_reqs = {
    "webapp_enabled": [
        "Flask==1.1.1",
    ],
    "authentication_enabled": [
        "Flask-Security==3.0.0",
        "bcrypt==3.1.7",
    ],
    "database_enabled": [
        "Flask-SQLAlchemy==2.4.1",
    ],
    "workers_enabled": [
        "flask-celery",
    ],
    "websockets_enabled": [
        "Flask-SocketIO==4.2.1",
        "gevent-websocket==0.10.1",
    ]
}


def generate_requirements(project_path):
    rv = []
    with open(os.path.join(project_path, 'shipka_config.json')) as f:
        shipka_config = json.load(f)
        for key in shipka_config:
            requirements = config_pip_reqs.get(key, [])
            rv += requirements
    return rv


if __name__ == "__main__":
    if sys.argv[1] == 'generate_requirements':
        requirements = generate_requirements(sys.argv[2])
        for r in requirements:
            print(r)
